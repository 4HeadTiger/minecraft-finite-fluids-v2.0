# Overview #

This mod seeks to add Finite Fluids to Minecraft. This mod requires Minecraft Forge for 1.7.10 (Latest recommended build: 10.13.4.1448)


### Getting Started with this repo ###

Very straight forward. Set up new workspace, and clone this repository into the /src/ folder. 

This mod uses some Forge Access Transformers, described in src/main/resources/META-INF/ff_at.cfg. To implement this, modify your gradle build script as described here; http://www.minecraftforge.net/wiki/Using_Access_Transformers#ForgeGradle_.2F_Minecraft_1.7.10

Silly me, I pushed from the /src/ folder and can't really be bothered fixing it.

### Features, TODO, etc ###

Features;

- All vanilla fluids are finite
- Fluid pressure
- Fluid interactions, heavier fluids displace lighter ones
- Custom multi-threaded block updater
- Custom client<=>Server block flagging system (may not be final)
- Commands and config file to control settings
- Water can be displaced by pistons and falling blocks
- Basic mod support and integration (Buildcraft, Glenn's Gases).

Todo

- Lots of bugfixing and cleaning
- Lots of compatibility tweaking
- Investigate migrating chunk flagging system to thread
- Investigate writing entirely new block update system
- Maybe implement gases (mostly just upside-down fluids :D)

Known issues;

- Fluids can cause some lighting issues.
- Fluids generate strange stone lines with Streams Mod (????)

### Using the Command ###

This mod adds a basic command to give some on-the-fly control over fluid behaviors. All fluid commands are accessed using "/fluids", with extra parameters.

These are the main commands;

- start/stop => pauses resumes all updates
- stats => shows current update tallies. "stats reset" to clear.
- pressure [true/false] => enables/disables pressure calculations
- rate [integer > 0] => Number of ticks per update sweep
- threads [integer > 0] => The number of threads to use
- load [float 0.0-1.0] => The amount of each tick to spend
- near [integer > 0] => Unskippable radius (recommended: 3-5]
- far [ingeger > 0] => The max update radius (depends on CPU)

All parameters in square brackets [], may be omitted to display the current value of the settings. All changes to settings with this command, will be flushed to the config file.

### Other ###

If you find any bugs, feel free to create a new issue in the Issue tracker.
https://bitbucket.org/4HeadTiger/minecraft-finite-fluids-v2.0/issues

Note: The master branch should always represent a stable build. The experimental branches represent my current progress with the mod, and may not be stable.

### Contact and Licensing ###

You can contact me on the Minecraft Forums, via PM;
http://www.minecraftforum.net/members/4HeadTiger

Or in the thread for this mod;
http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/wip-mods/2158282-realistic-fluids-overhaul-finite-water-and-stuff


## This mod is licensed under the GPLv3 ##