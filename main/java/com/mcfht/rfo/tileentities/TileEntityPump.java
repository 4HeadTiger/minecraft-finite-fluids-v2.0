package com.mcfht.rfo.tileentities;

import java.util.concurrent.locks.ReentrantLock;

import com.mcfht.rfo.RFO;
import com.mcfht.rfo.RFORegistry;
import com.mcfht.rfo.Settings;
import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.datastructure.DataInterfaces.IWorld;
import com.mcfht.rfo.datastructure.Manager;
import com.mcfht.rfo.utils.Coord;
import com.mcfht.rfo.utils.Util;
import com.mcfht.rfo.utils.Coord.Direction;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidTankInfo;
import net.minecraftforge.fluids.IFluidTank;

/** A simple base class for a pump-based tile entity. */
public class TileEntityPump extends TileEntity implements ITileEntityProvider
{
	/** The amount of fluid buffered within this pump*/
	public int buffer = 0;
	/** The tier of this pump. 0 = basic, 1 = tier 1, etcetera. */
	public int tier = 0;	
	
	public String fname;
	public Fluid f;
	
	public final ReentrantLock lock = new ReentrantLock(false);
	
	public TileEntityPump() {}
	
	public TileEntityPump(int type)
	{
		this.tier = type;
	}
	
	/** This method controls pretty much everything important about the rate at which this
	 * pump can operate. */
	public int maxDraw()
	{
		return (Settings.FLUID_LAYER << 1) + (tier * Settings.FLUID_LAYER);
	}
	
	@Override
	public void readFromNBT(NBTTagCompound tag)
    {
        super.readFromNBT(tag);
        this.buffer = tag.getInteger("buffer");
        this.tier = tag.getInteger("type");
        this.fname = tag.getString("fluidName");
        if (fname.equals("noFluid") == false)
        	this.f = FluidRegistry.getFluid(fname);
    }

	@Override
    public void writeToNBT(NBTTagCompound tag)
    {
    	super.writeToNBT(tag);
        tag.setInteger("buffer", this.buffer);
        tag.setInteger("type", this.tier);
        
        if (f != null) 
        	fname = f.getName();
        
        tag.setString("fluidName", fname == null ? "noFluid" : fname);
    }
    
	@Override
	public TileEntity createNewTileEntity(World w, int meta) {
		return new TileEntityPump();
	}

	
	@Override
	public void updateEntity() 
	{
		
		if (worldObj.isRemote) return;
		 
		if (worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord) == false 
		 && worldObj.isBlockIndirectlyGettingPowered(xCoord, yCoord + 1, zCoord) == false) return;
		
		
		int maxBuffer = maxBuffer();
		int m = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);
		int mDir = m & 0x7;

				
		//if we are not full, try to fill
		if (buffer < maxBuffer)
		{
			//grab from the opposite direction in which the pump points
			
			int x1 = xCoord + Util.getFace(mDir ^ 0x1)[0];
			int y1 = yCoord + Util.getFace(mDir ^ 0x1)[1];
			int z1 = zCoord + Util.getFace(mDir ^ 0x1)[2];

			//if it's within the bounds of the world
			if (y1 >= 0 && y1 <= 255)
			{
				Block b1 = worldObj.getBlock(x1, y1, z1);
				if (b1 instanceof BlockFiniteFluid)
				{
					//if it's a fluid that matches this pump
					BlockFiniteFluid f1 = (BlockFiniteFluid)b1;
					//only if the fluids match
					if (this.f == null || f1.ffluid.fluid == this.f)
					{

						IWorld w = Manager.instance.getOrRegisterWorld(worldObj);
						IChunk d = w.getChunk(x1 >> 4, z1 >> 4);
						//only if the chunk is loaded/registered (should usually be true)
						if (d != null)
						{
							//get the fluid in f1
							int l1 = d.getFluid(b1, -1, x1 & 0xF, y1, z1 & 0xF);
							//now calculate how much we can move into the pump
							int toMove = Math.min(maxDraw(),  maxBuffer - buffer);
							
							//shift the appropriate amount of fluid
							l1 -= toMove;
							buffer += toMove;
							
							//update the fluid content
							w.setFluid(d, x1, y1, z1, b1, -1, f1, l1, 0x7);
							f = f1.ffluid.fluid;
						}
					}
				}
			}
		}
		
		if (buffer > 0 && RFO.sweepCounter % Settings.TICK_CURRENT == 0) 
		{
			
			//step to the block we are facing
			int x1 = xCoord + Util.getFace(mDir)[0];
			int y1 = yCoord + Util.getFace(mDir)[1];
			int z1 = zCoord + Util.getFace(mDir)[2];
			
			//only if it's within the bounds of the world
			if (y1 > 0 && y1 < 255)
			{
				//get the chunk information
				IWorld w = Manager.instance.getOrRegisterWorld(worldObj);
				IChunk d = w.getChunk(x1 >> 4, z1 >> 4);
				if (d != null)
				{
					//get our block
					Block b1 = d.getBlock(x1 & 0xF, y1, z1 & 0xF);
					boolean can = false;
					
					//try to retrieve any fluid
					int l1 = 0;
					if (b1 instanceof BlockFiniteFluid)
					{
						BlockFiniteFluid f0 = (BlockFiniteFluid)b1;
						if (f0.ffluid.fluid == f)
						{
							l1 = d.getFluid(b1, -1, x1 & 0xF, y1, z1 & 0xF);
							can = true;
						}
					}
					
					//now, we are the same fluid, or we are air, fill the block infront of us
					if (can || b1 == Blocks.air && f != null)
					{
					
						int toMove = Math.min(maxDraw(), Settings.FLUID_MAX - l1);
						l1 += toMove;
						buffer -= toMove;
						
						if (l1 > 0)
						{
							w.setFluid(d, x1, y1, z1, b1, -1, (BlockFiniteFluid)RFORegistry.getFromFluid(f).getAppropriateBlockType(l1), l1, 0x7);
						}
					}
				}	
			}
			
			//ensure our metadata is valid
			if ((m & 0x8) != 1)
				worldObj.getChunkFromBlockCoords(xCoord, zCoord).setBlockMetadata(xCoord & 0xF, yCoord, zCoord & 0xF, m | 0x8);
		}
		
		//we are empty, clear the stuff here
		if (buffer <= 0)
		{
			worldObj.getChunkFromBlockCoords(xCoord, zCoord).setBlockMetadata(xCoord & 0xF, yCoord, zCoord & 0xF, m & 0x7);
			f = null;
		}
		
		
	}
	
	
	public final int maxBuffer()
	{
		return maxDraw() << 1;
	}

	
	public final int maxFlow()
	{
		return Math.min(buffer, maxDraw());
	}
	
	
	/***
	 * Tests how much fluid can be retrieved from this buffer, up to the specified maximum.
	 * @param d
	 * @param max
	 * @return The maximum amount of fluid that can be retrieved, up to the specified amount.
	 */
	public int maxFromBuffer(IChunk d)
	{
		return Math.min(buffer, Math.min(maxFlow(), Settings.FLUID_MAX));
	}
	
	
	/***
	 * Attempts to pull the specified maximum amount of fluid out of this entity's fluid buffer.
	 * @param d
	 * @param max
	 * @return The maximum amount of fluid that can be retrieved, up to the specified amount.
	 */
	public int pullFromBuffer(IChunk d)
	{
		try
		{
			lock.lock();
			int canPull = Math.min(buffer, Math.min(maxFlow(), Settings.FLUID_MAX));
			buffer = Math.max(0, buffer - maxFlow());
			return canPull;
		}
		finally { lock.unlock(); }
	}

	//@Override
	public FluidStack getFluid() {
		if (f == null) return null; else return new FluidStack(f, getFluidAmount());
	}

	//@Override
	public int getFluidAmount() {
		return (int)((float)buffer / 8.192F);
	}

	//@Override
	public int getCapacity() {
		return 1000;
	}

	//@Override
	public FluidTankInfo getInfo() {
		return new FluidTankInfo(null);
	}

	//@Override
	public int fill(FluidStack resource, boolean doFill) {
		if (buffer < Settings.FLUID_MAX)
		{
			if (resource != null && (f == null || resource.fluid == this.f))
			{
				if (doFill)
				{
					buffer += (int)((float)resource.amount * 8.192F);
				}
			}
		}
		return Settings.FLUID_MAX - buffer;
	}

	//@Override
	public FluidStack drain(int maxDrain, boolean doDrain) 
	{
		if (f == null || buffer <= 0) return null;
		
		FluidStack fs = new FluidStack(f, Math.min(maxDrain, getFluidAmount()));
		
		if (doDrain)
		{
			buffer -= Math.min(maxDrain, getFluidAmount());
			if (buffer <= 0)
			{
				buffer = 0;
				f = null;
			}
		}
		
		return fs;
	}
	
	
	
	
	
}
