package com.mcfht.rfo.compatibility;

import com.mcfht.rfo.RFO;
import com.mcfht.rfo.utils.Util;

public class Patch
{
	/** The qualified path to a class that this patch will look for. */
	public final String className;
	/** The name of the mod to associate with this patch (for display purposes only atm). */
	public final String name;
	
	/** Make a new patch. BaseClass is just 'some class that will only exist if the target mod is installed.
	 * nameOfMod is simply, any name you want. Can be the modID, doesn't have to be.*/
	public Patch(String nameOfMod, String baseClass)
	{
		className = baseClass;
		name = nameOfMod;
	}
	
	/** This method should not be overridden unless you know what you are doing.
	 * Basically just ensures that the target class (aka mod) is accessible.*/
	public boolean checkCompatibility()
	{
		try
		{
			if (installed()) return false;
			Class.forName(className);
			Util.info(name + " detected!");
			return performNecessarySteps();
		}
		catch (Exception e)	{ return false;	}
	}
	
	/** Override this method. When this method is run, the parent class target for this patch is present. Return: true if success.
	 * Note that the return isn't really used for much. 
	 * @throws Exception */
	
	public boolean performNecessarySteps() throws Exception
	{
		return false;
	}

	/** Whether this patch has installed already (i.e, if it still needs to be processed) */
	public boolean installed()
	{
		return false;
	}
	
	
}
