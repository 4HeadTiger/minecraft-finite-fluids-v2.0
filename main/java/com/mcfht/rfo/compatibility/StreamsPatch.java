package com.mcfht.rfo.compatibility;

import com.mcfht.rfo.blocks.BlockFiniteFluid;

public class StreamsPatch extends Patch {

	public static boolean HAS_STREAMS = false;
	
	public StreamsPatch() {
		super("Streams Mod", "streams.block.BlockRiver");
	}

	@Override
	public boolean performNecessarySteps() throws Exception
	{
		BlockFiniteFluid.classBlockRiver = Class.forName("streams.block.BlockRiver");
		HAS_STREAMS = true;
		return true;
	}
	
}
