package com.mcfht.rfo.compatibility;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import com.mcfht.rfo.utils.Coord;
import com.mcfht.rfo.utils.Util;

import net.minecraft.world.World;

public class GlennsGases extends Patch {

	private static Class GFAPI;
	private static Class gasType;
	private static Field steamGas;
	private static Method placeGas;
	
	private static int amount = 0;
	
	public static boolean INSTALLED = false;

	/** Makes a new patch. The parameter toggles two possible names for the GFAPI parent class. */
	public GlennsGases(int var) {
		super("Glenn's Gasses", var == 0 ? "glenn.gasesframework.api.GasesFrameworkAPI" : "glenn.gasesframework.api.GFAPI");
	}

	
	@Override
	public boolean performNecessarySteps() throws Exception 
	{
		//cbf using the API. Reflection should be fine.
		
		//"glenn/gasesframework/api/GFAPI", "implementation", "Lglenn/gasesframework/api/IGFImplementation;"
		GFAPI = Class.forName("glenn.gasesframework.api.GasesFrameworkAPI");
		steamGas = Class.forName("glenn.gases.Gases").getDeclaredField("gasTypeSteam");
		gasType = steamGas.getClass();

		for (Method m : GFAPI.getDeclaredMethods())
		{
			if (m.getName().equals("placeGas") && m.getParameterCount() == 6) placeGas = m;
		}
		if (placeGas == null) throw new Exception("Not find method :D");
		
		//TODO: Get the correct value
		amount = 16;
		
		Util.info(" - Hooked steam placement");
		INSTALLED = true;
		
		return true;
	}
	
	public static void placeSteam(World w, int x, int y, int z, int amount)
	{
		if (INSTALLED == false) return;

		try { placeGas.invoke(GFAPI, w, x, y, z, steamGas.get(null), amount);
		Util.info("Placed GG Steam: " + new Coord(x, y, z) + " amount: " + amount);
		}catch (Exception e) {
			Util.error("Failed to place Steam. Reason: " + e.getMessage());
			return; 
		}
	}
	
	public boolean installed()
	{
		return INSTALLED;
	}
	
	
}
