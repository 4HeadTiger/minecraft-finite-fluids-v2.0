package com.mcfht.rfo;

import java.util.Map;

import org.apache.logging.log4j.LogManager;

import com.mcfht.rfo.asm.ASMTransformer;
import com.mcfht.rfo.utils.Util;

import cpw.mods.fml.relauncher.IFMLLoadingPlugin;
import cpw.mods.fml.relauncher.IFMLLoadingPlugin.*;

@MCVersion("1.7.10")
//Adds compatibility with a few mods
@IFMLLoadingPlugin.SortingIndex(1002) //all credit goes to Keybounce for figuring this one out
public class LaunchWrapper implements IFMLLoadingPlugin{

    public static final String MODID = RFOInfo.MODID;
    public static final String VERSION = RFOInfo.VERSION;
	static int done = 0;
	@Override
	public String[] getASMTransformerClass() 
	{
		if (done++ != 0) Util.heading(RFOInfo.MODID + " is running! Version: " + RFOInfo.VERSION);
		return new String[]{ ASMTransformer.class.getName() };
	}

	@Override
	public String getModContainerClass() 
	{
		return RFO.class.getName();
	}

	@Override
	public String getSetupClass() 
	{
		return null;
	}

	@Override
	public void injectData(Map<String, Object> data) 
	{
	}

	@Override
	public String getAccessTransformerClass() 
	{
		return null;
	}

}
