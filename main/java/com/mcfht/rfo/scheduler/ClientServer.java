package com.mcfht.rfo.scheduler;

import java.util.concurrent.ConcurrentLinkedDeque;

import com.mcfht.rfo.Settings;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.scheduler.UpdaterInterfaces.IClientUpdater;
import com.mcfht.rfo.scheduler.deferred.BlockUpdate;
import com.mcfht.rfo.scheduler.deferred.DropBlock;
import com.mcfht.rfo.scheduler.deferred.ServerTask;
import com.mcfht.rfo.utils.Coord;

import io.netty.util.internal.ConcurrentSet;
import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.chunk.storage.ExtendedBlockStorage;

public class ClientServer implements IClientUpdater {

	
	public ConcurrentSet<IChunk> chunks = new ConcurrentSet<IChunk>();
	
	@Override
	public void markChunkDirty(IChunk c) 
	{
		chunks.add(c);
	}
	
	@Override
	public void processChunk(IChunk d, int dist, int unused1, int unused2) 
	{
		d.updateDist(dist);
		if (d.hasClientUpdates()) markChunkDirty(d);
	}

	@Override
	public boolean testChunkDist(IChunk d) 
	{
		if (d.getDist() <= 2) return true; //always update 9 chunks around each player
		int pastCounter = d.getDist() / Settings.UPDATE_REDUCTION_FALLOFF;
		return d.ticksPassed() > pastCounter; //
	}

	@Override
	public boolean testChunkForSending(IChunk d) 
	{
		if (testChunkDist(d) == false) return false;
		if (d.prepareForSending() == false) return false;
		
		//System.out.println("  - Sending is okay!");
		return true;

	}
	
	@Override
	public void sendChunk(IChunk d)
	{
		d.acquire(); //be safe, wait until after the chunk has updated etc
		chunks.remove(d); //un store it.
		d.chunkSending();

		boolean[][] updates = d.getBlockFlags();
		
		if (updates == null) return;

		//this object will help unravel the coordinates
		Coord c = new Coord(0);
		int xx = d.xPos() << 4;
		int zz = d.zPos() << 4;
		
		//int tickCounter = 0;
		
		for (int ey = 0; ey < 16; ey++)
		{
			if (updates[ey] == null) continue;
			//ExtendedBlockStorage ebs = d.getEbs(ey, true);
			
			for (int i = 0; i < 4096; i++)
			{
				//there is an update flagged at this location.
				//the server will do the job of figuring out which block :D
				if (updates[ey][i])
				{
					c.set(i); //unravel the coordinates
					d.getIWorld().getWorld().markBlockForUpdate(c.x + xx, c.y + (ey << 4), c.z + zz);
					updates[ey][i] = false; //to be safe
					//tickCounter++;
				}
			}
			updates[ey] = null; //to be safer
		}
		//System.out.println(" <== Done. Flagged: " + tickCounter + " blocks!");
		d.release(); //done
	}
	
	
}
