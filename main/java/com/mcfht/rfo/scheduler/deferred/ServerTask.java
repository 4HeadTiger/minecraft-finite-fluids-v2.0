package com.mcfht.rfo.scheduler.deferred;

import java.util.concurrent.ConcurrentLinkedQueue;

/** A basic framework for tasks that need to be performed on the server tick (rather than threads). 
 * 
 * <p>Each task should register itself in the constructor, after all relevant fields are constructed.
 * Registering a task into the queue before it is ready may cause serious problems.
 * 
 * @author FHT
 */
public class ServerTask {

	public static ConcurrentLinkedQueue<ServerTask> tasks = new ConcurrentLinkedQueue<ServerTask>();

	/** Performs this task. Extend and provide custom behavior  */
	public void perform()
	{
		
	}
	
	
	public static final void doDeferredTasks()
	{
		while (tasks.isEmpty() == false)
		{
			tasks.poll().perform();
		}
	}
	
}
