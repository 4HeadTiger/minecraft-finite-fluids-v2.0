package com.mcfht.rfo.scheduler.deferred;

import com.mcfht.rfo.Settings;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.utils.Util;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class BlockIgnition extends ServerTask {

	
	World w;
	int x, y, z, flammability;
	float level;
	
	public BlockIgnition(World w, int x, int y, int z, int flammability, int level)
	{
		this.w = w;
		this.x = x; 
		this.y = y; 
		this.z = z; 
		this.flammability = flammability;
		this.level = (float)level/(float)Settings.FLUID_MAX;
		tasks.add(this);
	}
	
	
	@Override
	public void perform() 
	{

		int count = 0, air = 0;
		for (int xx = x - 2; xx < x + 3; xx++)
		{
			for (int yy = y - 2; yy < y + 3; yy++)
			{
				for (int zz = z - 2; zz < z + 3; zz++)
				{
					count++;
					Block bN = w.getBlock(xx, yy, zz);
					if (!bN.getMaterial().blocksMovement() && bN.getMaterial() != Material.water && bN.getMaterial() != Material.lava) air++;
				}
			}
		}
		
		float airProportion = (float)air/(float)count;
		
		//if there is not much air, just pretend we are under pressure and explode
		if (airProportion < 0.33F)
		{
			//explode
			float power = Math.max(0.5F, Math.min(flammability * level * level, 2F));
			w.newExplosion((Entity)null, (double)x + 0.5D, (double)y + 0.5D, (double)z + 0.5D, power, true, true);
		}
		
		//now make fire in the space
		int bound = airProportion < 0.33F ? 5 : 3;
		int repeat = (int) Math.sqrt(airProportion < 0.33F ? flammability * 1.25 : flammability); //make it odd, always at least one
		
		for (int i = 0; i < repeat; i++)
		{
			int x1 = x + Util.rand.nextInt(bound) - (bound - 2);
			int y1 = y + Util.rand.nextInt(bound) - (bound - 2);
			int z1 = z + Util.rand.nextInt(bound) - (bound - 2);
			
			//try to put fire here
			if (w.getBlock(x1, y1, z1) == Blocks.air){
				w.setBlock(x1, y1, z1, Blocks.fire);
			}
		}
	}
	
	
}
