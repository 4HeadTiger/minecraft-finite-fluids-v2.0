package com.mcfht.rfo.scheduler.deferred;

import com.mcfht.rfo.compatibility.GlennsGases;

import net.minecraft.init.Blocks;
import net.minecraft.world.World;

/** Creates steam, deferred action. */
public class CreateSteam extends ServerTask 
{

	int x, y, z, amount;
	World w;
	
	public CreateSteam(World w, int x, int y, int z, int amount)
	{
		if (GlennsGases.INSTALLED)
		{
			this.w = w;
			this.x = x;
			this.y = y;
			this.z = z;
			this.amount = amount;
			tasks.add(this);
		}
	}
	
	@Override
	public void perform() 
	{
		if (w.getBlock(x, y + 1, z) == Blocks.air)
			GlennsGases.placeSteam(w, x, y + 1, z, amount);
		else
		{
			//try and put it in the surrounding area
			for (int xx = x - 1; xx <= x + 1; xx++)
			{
				for (int zz = z - 1; zz <= z + 1; zz++)
				{
					for (int yy = y - 1; yy <= y + 2; yy++)
					{
						if (w.getBlock(xx, yy, zz) == Blocks.air)
						{
							GlennsGases.placeSteam(w, xx, yy, zz, amount);
							return;
						}
					}
				}
			}
		}
	}
	
	
	
}
