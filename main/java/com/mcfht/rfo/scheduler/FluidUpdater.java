package com.mcfht.rfo.scheduler;

import java.util.Collection;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

import com.mcfht.rfo.RFO;
import com.mcfht.rfo.Settings;
import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.datastructure.Manager;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.datastructure.DataInterfaces.IWorld;
import com.mcfht.rfo.scheduler.UpdaterInterfaces.IFluidUpdater;
import com.mcfht.rfo.scheduler.UpdaterInterfaces.IThreadManager;
import com.mcfht.rfo.scheduler.UpdaterInterfaces.ITicker;
import com.mcfht.rfo.utils.Coord;
import com.mcfht.rfo.utils.Util;

import io.netty.util.internal.ConcurrentSet;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;

public class FluidUpdater implements IFluidUpdater, IThreadManager 
{

	public static final FluidUpdater instance = new FluidUpdater();
	
	/** Set of near updates. These should always be performed, TODO: make the near update system ordered. */
	public ConcurrentSet<IChunk> near = new ConcurrentSet<IChunk>();	
	
	/** This set ensures no duplicate entries. Consistency between the queue
	 * and the set is not overly vital - errors will not cause serious problems. */
	public ConcurrentSet<IChunk> _far = new ConcurrentSet<IChunk>();
	/** Meanwhile this queue ensures that far updates happen vaguely in order. Consistency between the queue
	 * and the set is not overly vital - errors will not cause serious problems.  */
	public ConcurrentLinkedDeque<IChunk> ffar = new ConcurrentLinkedDeque<IChunk>();
	// ^ This is the far-queue LOL (say it 5 times really fast).

	public static boolean ENDING = false;
	
	public boolean addFar(IChunk c)
	{
		if (_far.add(c)) 
		{
			ffar.add(c);
			return true;
		}
		return false;
	}
	
	public void removeFar(IChunk c)
	{
		if (_far.remove(c))
		{
			ffar.remove(c);
		}
	}
	
	
	public void worldEnding(World w)
	{
		ENDING = true;
	}
	
	public void resume()
	{
		ENDING = false;
	}

	

	
	@Override
	public void flagUpdates(Coord c, IChunk d) 
	{
		int xx = (d.xPos() - c.x),
			zz = (d.zPos() - c.z),
			dist = xx * xx + zz * zz;
		
		RFO.sender.processChunk(d, dist, 0, 0);

		if (d.hasFluidUpdates())
		{
			if (c.y < 0)
			{
				if (dist <= Settings.RANGE_NEAR) { near.add(d); removeFar(d); }
				else if (dist <= Settings.RANGE_FAR && !near.contains(d)) addFar(d);
			}
			else if (dist < c.y * c.y && !near.contains(d))
			{
				addFar(d);
			}
		}
	}
	
	/** The number of active threads */
	public AtomicInteger activeThreads = new AtomicInteger(0);
	
	/** A collection containing all active threads. */
	public ConcurrentSet<FluidWorker> workers = new ConcurrentSet<FluidWorker>();
	
	@Override
	public Collection<IChunk> getUpdates(int priority) {
		return priority == 0 ? near : _far;
	}
	
	public ReentrantLock lock = new ReentrantLock(false);
	
	//just some boring counters
	/** The number of chunks that have been processed. */
	public static AtomicLong chunksDone = new AtomicLong(0);

	
	@Override public void runWorkers()
	{
		
		if (ENDING) return;
		

		lock.lock();
		
		try
		{
			//PLACE HOLDER: DO ALL NEAR, THEN DO FAR FOR AS LONG AS WE CAN
			for (int i = 0; i < Settings.UPDATER_COUNT - getActiveThreadCount(); i++)
			{
				//Stop if we are running a lot overtime
				if (near.isEmpty() && System.currentTimeMillis() - RFO.startTime > Settings.LOAD_MULT * Settings.TICK_CURRENT)
				{
					return;
				}
				
				//get the next update, and try to perform it on a new thread.
				IChunk C = getNextUpdate();
				if (C != null)
				{
					FluidWorker w = new FluidWorker(this, C);
					if (workers.add(w))
					{
						activeThreads.incrementAndGet();
						w.start();
					}
				}
			}
		}
		finally { lock.unlock(); }
	}
	
	@Override
	public void notifyChunkComplete(FluidWorker w, IChunk c) 
	{
		//?
		
		RFO.doRandomTicks(c);
		
		chunksDone.incrementAndGet();

		lock.lock();
		
		try
		{

			if (workers.remove(w))
				activeThreads.decrementAndGet();
			else activeThreads.set(workers.size());	
			
			
			//Stop if we are running a lot overtime
			if (near.isEmpty() && System.currentTimeMillis() - RFO.startTime > Settings.LOAD_MULT * Settings.TICK_CURRENT)
			{
				return;
			}
			
			//get the next update, and try to perform it on a new thread.
			IChunk C = getNextUpdate();
			if (C != null)
			{
				w.c = C;
				if (workers.add(w))
				{
					activeThreads.set(workers.size());
					lock.unlock();
				}
				w.doAction();
			}
			//start new threads
			//runWorkers();
		}
		finally
		{
			while (lock.isHeldByCurrentThread()) lock.unlock();
		}
	}
	
	/** Get the next update to perform. Returns near first, then far when all the near updates
	 * are completed. Null if no updates. */
	public IChunk getNextUpdate()
	{
		if (near.iterator().hasNext())
		{
			IChunk C = near.iterator().next();
			near.remove(C);
			return C;
		}
		else if (ffar.isEmpty() == false)
		{
			IChunk C = ffar.poll();
			if (C != null) _far.remove(C);
			return C;
		}
		else return null;
	}

	@Override
	public int getActiveThreadCount() {
		return activeThreads.get();
	}

	@Override
	public Collection<FluidWorker> getWorkers() {
		return workers;
	}



}
