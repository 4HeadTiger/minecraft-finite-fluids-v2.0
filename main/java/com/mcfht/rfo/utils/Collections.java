package com.mcfht.rfo.utils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.function.IntUnaryOperator;

import com.mcfht.rfo.Settings;
import com.mcfht.rfo.blocks.BlockFiniteFluid;


public class Collections 
{
	
	/** For prototyping reasons */
	public static class SpecialStack { }

	
	/**
	 * A bootleg no-duplicates queue. Optimized around 16x16x16 coordinate grid, uses boolean array
	 * to flag existing entries. Works mostly like a regular queue outside of this.
	 * @author FHT
	 *
	 */
	public static class ShortQueueSet extends SpecialStack
	{
		/** the redundancy array */
		protected boolean flags[] = new boolean[4096];
		/** The items in this stack */
		protected Queue<Short> items = new LinkedList<Short>();
				
		/**
		 * Lowest adder. Uses {@link Util#getIndex12(int, int, int)} indices.
		 * @param index
		 */
		public void push(int index)
		{
			if (flags[index]) return;
			flags[index] = true;
			items.add((short)index);
		}
		
		/**
		 * Adds the given chunk coord to this stack.
		 * @param cx
		 * @param cy
		 * @param cz
		 */
		public void push(int cx, int cy, int cz)
		{
			push(Util.getIndex12(cx, cy, cz));
		}
		
		/**
		 * Adds the provided coordinates to this stack. Will clamp within 0-15 on all coordinates.
		 * @param coords
		 */
		public void push(Coord coords)
		{
			push(Util.getIndex12(coords.x & 0xF, coords.y & 0xF, coords.z & 0xF));
		}
		
		/**
		 * Returns the last integer added to this stack set. Follows the encoding described by
		 * {@link Util#getIndex12(int, int, int)}.
		 * @return
		 */
		public int pop()
		{
			int i = (int)items.poll().shortValue();
			flags[i] = false;
			return i;
		}
		/**
		 * Returns the last coordinate added to this stack, using CoordTuple. Coordinate is described
		 * in the context of ebs (all coordinates &= 0xF).
		 * @return
		 */
		public Coord getNextCoord()
		{
			int i = pop();
			return i < 0 ? null : new Coord(i);
		}
		
		/**
		 * Returns the items stored in this stack in a new array (trimmed to size).
		 * @return
		 */
		public Object[] toArray()
		{
			return items.toArray();
		}
		
		/** Returns the number of items in this stack */
		public int size() { return items.size(); }
	}
	

	
	/**
	 * A simplified primitive stack working with shorts. Works like a regular stack,
	 * but very lightweight.
	 * @author FHT
	 *
	 */
	public static class ShortStack extends SpecialStack
	{
		protected short[] items;
		public int count;
		
		public ShortStack(int size)
		{
			items = new short[size];
			count = 0;
		}
		
		/** Adds new (short)int to the head of the stack */
		public void push(int i) { push((short)i); }
		/** Adds new short to the head of the stack */
		public void push(short i)
		{
			if (items.length >= count)
			{
				short[] a = new short[items.length + 256];
				System.arraycopy(items, 0, a, 0, count);
				items = a;
			}
			items[count++] = i;
		}
		
		/** Removes the item at the head of the stack */
		public int pop()
		{
			if (count > 0) return items[--count];
			return -1;
		}
		
		/** Copies the items of this stack into an array. truncates empty elements. */
		public short[] toArray()
		{
			return Arrays.copyOf(items, count);
		}
		
		/** Returns the number of items in this stack */
		public int size() { return count; }
		
		public Stack s;
	}
	
	/**
	 * A simplified primitive stack working with shorts. Works like a regular stack,
	 * but very lightweight.
	 * @author FHT
	 *
	 */
	public static class ByteStack extends SpecialStack
	{
		protected byte[] items;
		protected int count;
		
		public ByteStack(int size)
		{
			items = new byte[size];
			count = 0;
		}
		
		/** Adds new short to the head of the stack */
		public void push(byte i)
		{
			if (items.length >= count)
			{
				byte[] a = new byte[items.length + 256];
				System.arraycopy(items, 0, a, 0, count);
				items = a;
			}
			items[count++] = i;
		}
		
		/** Removes the item at the head of the stack */
		public byte pop() throws IllegalStateException
		{
			if (count > 0) return items[--count];
			throw new IllegalStateException("Attempting to pop from empty stack");
		}
		
		/** Copies the items of this stack into an array. Trims empty elements. */
		public byte[] toArray()
		{
			return Arrays.copyOf(items, count);
		}
		
		/** Returns the number of items in this stack */
		public int size() { return count; }
	}
	
	
}
