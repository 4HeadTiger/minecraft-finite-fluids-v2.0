package com.mcfht.rfo.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mcfht.rfo.RFO;
import com.mcfht.rfo.Settings;
import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;

import net.minecraft.block.Block;
import net.minecraft.world.chunk.Chunk;

/**
 * This class contains some boring Util stuff for general use throughout the mod. Changing functionality here may break
 * a number of things.
 * 
 * <p>This class probably does not follow any logical order. Functions are added as required.
 * @author FHT
 *
 */
public class Util 
{

	public static Logger ffLogger = LogManager.getLogger("RFO");

	
	public static int entropy = 0;
	/** Returns an appropriately random number from 0-3 (ideal use: {@link Util#dirs_hCardinal})
	 * <p>This method is much faster than regular {@link Random#nextInt()} method
	 */
	public static int randomDirVal4()
	{
		return (entropy++) & 0x3;
	}
	/** Returns an appropriately random number from 0-8  (ideal use: {@link Util#dirs_hCorner})
	 * <p>This method is much faster than regular {@link Random#nextInt()} method
	 */
	public static int randomDirVal8()
	{
		return (entropy++) & 0x7;
	}
	
	public static boolean randBool(int n)
	{
		return ((entropy++) & n) == 0;
	}
	/**
	 * Random generator used by most of the mod.
	 */
	public static final Random rand = new Random();
	
	/**
	 * The four cardinal directions as dx and dz.
	 * <p>	{0, 1}, {0, -1}, {-1, 0}, {1, 0}
	 * <p> dirs[n ^ 0x1] gets reverse direction.
	 */
	protected static final int[][] dirs_hCardinal =
		{
			{0, 1}, {0, -1}, {-1, 0}, {1, 0}
		};
	
	/**
	 * The four corner directions as dx and dz.
	 * <p>	{-1, 1}, {1, -1}, {-1, -1}, {1, 1}
	 * <p> Each corner is anti-clockwise from the direction in {@link Util#dirs_hCardinal}.
	 */
	protected static final int[][] dirs_hCorner =
		{
			{-1, 1}, {1, -1}, {-1, -1}, {1, 1}
		};
	
	/** The 6 facesReversible of blocks. Directions align with ForgeDirection.*/
	public static final int[][]	faces =
		{
			{0, -1, 0},
			{0, 1, 0},
			{0, 0, -1},
			{0, 0, 1},
			{-1, 0, 0},
			{1, 0, 0},
		};
		
	/**
	 * Order: Below, around, above
	 * @param dir
	 * @return
	 */
	public static final int[] getFace(int dir)
	{
		return faces[dir % 6];
	}
	

	/**
	 * Gets a direction on the horizontal cardinal axes. Ordering described by {@linkplain Util#dirs_hCardinal}.
	 * @param dir
	 * @return
	 */
	public static final int[] getDir(int dir)
	{
		return dirs_hCardinal[dir & 0x3];
	}
	
	/**
	 * Gets a corner. Ordering described by {@linkplain Util#dirs_hCorner}.
	 * @param dir
	 * @return
	 */
	public static final int[] getCorner(int dir)
	{
		return dirs_hCorner[dir & 0x3];
	}
	
	
	/**
	 * Gets a direction, including diagonals. Ordering is unbound, starts with {@linkplain Util#dirs_hCardinal} from 0-3, and
	 * then moves to from 4-7. {@linkplain Util#dirs_hCorner}.
	 * @param dir
	 * @return
	 */
	public static final int[] getDir8(int dir)
	{
		return dir >=4 ? getCorner(dir) : getDir(dir);
	}
	
	/**
	 * Calculates a 12 bit index (0-4095) from three 4-bit ebs coordinates.
	 * 
	 * <p>Encoding: <code>cx | (cz << 4) | (cy << 8);
	 * @param cx
	 * @param cy 
	 * @param cz
	 * @return
	 */
	public static final int getIndex12(int cx, int cy, int cz)
	{
		return cx | (cz << 4) | (cy << 8);
	}
	
	
	/**
	 * Makes a list from the parameters or array provided.
	 */
	public static <T> List<T> makeList(T... a)
	{
		List<T> l = new ArrayList<T>(a.length);
		for (T i : a) l.add((T) a);
		return (ArrayList<T>) l;
	}
	
	/** Log base 2*/
	public static int log2(int arg)
	{
	    if( arg == 0 )
	        return 0; // or throw exception
	    return 31 - Integer.numberOfLeadingZeros( arg );
	}
	
	/** Attempts to find any blocks with this name.
	 * 
	 * @param exact If true, finds only exact matches. If false, finds anything containing the provided string.
	 * @return
	 */
	public Collection<Block> findBlocks(String name, boolean exact)
	{
		List<Block> b = new ArrayList<Block>();
		for (Object o : Block.blockRegistry.getKeys())
		{
			if ((exact && o.toString().equals(name)) || o.toString().contains(name))
			{
				b.add(Block.getBlockFromName((String)o));
			}
		}
		return b;
	}
	
	
	static boolean inheader = false;
	static String hString = "";
	static int hlen = 0;
	
	private static String getBarLen(String s, int pad)
	{
		int l = Math.max(5, 11 - (s.length()/7)) + pad;
		String n = "";
		while (n.length() < l) n += "=";
		return n;
	}
	
	public static void heading(String s)
	{
		String bars = getBarLen(s, 0);
		s = " " + bars + "> " + s + " <" + bars;
		hlen = s.length();
		ffLogger.info(s);
	}
	
	public static void enterSection(String s, int pad)
	{
		inheader = true;
		hString = s;
		String bars = getBarLen(s, pad);
		s = " " + bars + "> " + s + " <" + bars;
		hlen = s.length();
		ffLogger.info(s);
	}
	public static void enterSection(String s) { enterSection(s, 0); }
	
	public static void info(String s)
	{
		if (inheader)
		{
			s = " => " + s;
			while (s.length() < hlen - 3) s += " ";
			s += " <=";
		}
		ffLogger.info(s);
	}
	
	public static void error(String s)
	{
		if (inheader)
		{
			s = " => " + s;
			while (s.length() < hlen - 3) s += " ";
			s += " <=";
		}
		ffLogger.error(s);
	}
	
	
	public static void leaveSection()
	{
		inheader = false;
		String s = " ";
		while (s.length() < hlen) s += "=";
		hlen = 0;
		ffLogger.info(s);
	}

}
