package com.mcfht.rfo.utils;

import com.mcfht.rfo.scheduler.FluidWorker;

/**
 * The parent class for all update workers.
 * @author FHT
 *
 */
public class Worker extends Thread
{
	public volatile boolean sleeping;
	public final int id; public long startTime;
	public Worker(int ID) { id = ID; }
	
	/**
	 * This method is not to be overridden. Used to handle threads and interact with
	 * the thread management stuffs.
	 * 
	 * <p>To specify the threaded behavior, override {@link Worker#doActions()}.
	 * <hr>
	 * {@inheritDoc}
	 * */
	@Override
	public final void run() 
	{
		//FluidUpdater.tryStartThread(this);
		startTime = System.currentTimeMillis();
		doAction();
		//IPool.instance.notifyThreadEnd(this);
	}
	/** This method is run when this thread is started. To enter this method, call {@link FluidWorker#start()}.
	 * The FluidWorker class will automatically handle thread allowances and so on. Treat like {@link Thread#run()}. 
	 */
	public void doAction() {}
}
