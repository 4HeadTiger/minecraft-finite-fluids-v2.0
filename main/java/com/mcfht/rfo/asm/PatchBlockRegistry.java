package com.mcfht.rfo.asm;

import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;

import com.mcfht.rfo.RFO;
import com.mcfht.rfo.asm.ASMTransformer.PatchTask;
import com.mcfht.rfo.utils.Util;

import net.minecraft.block.Block;

/** Patches the vanilla block registry. 
 @author FHT */
public class PatchBlockRegistry extends PatchTask
{

	public PatchBlockRegistry() { super(-1, NAMES, null); }
	public static final String[] NAMES = { "net.minecraft.block.Block", "aji" };
	public static final String blockOwner = "com/mcfht/rfo/RFO";
	public static final  String[] blockNames = { "com/mcfht/rfo/blocks/BlockFiniteWater",
												 "com/mcfht/rfo/blocks/BlockFiniteLava" };
	@Override
	public void doPatch(String name, ClassNode c, boolean obf) 
	{	
		Util.enterSection("Patching Block Registry");
		int counter = 0;
		@SuppressWarnings("unchecked")
		Iterator<MethodNode> methods = c.methods.iterator();
		for (MethodNode m : c.methods)
		{
			AbstractInsnNode node0 = null;
			@SuppressWarnings("unchecked")
			Iterator<AbstractInsnNode> iter = m.instructions.iterator();
			int index = -1;
			int startIndex = -1;
			while (iter.hasNext())
			{
				index++;
				node0 = iter.next();
				if (node0.getOpcode() == org.objectweb.asm.Opcodes.LDC && iter.hasNext())
				{
					startIndex = index++;
					AbstractInsnNode node1 = iter.next();
					if (node1.getOpcode() != Opcodes.NEW) continue;
					LdcInsnNode target = (LdcInsnNode) node0;

					InsnList toRemove = new InsnList();
					toRemove.add(node1);
					//Store the starting node and starting index
					if (target.cst.equals("water") || target.cst.equals("flowing_water"))
					{
 						((TypeInsnNode)m.instructions.get(startIndex+1)).desc = blockNames[0];
						((MethodInsnNode)m.instructions.get(startIndex+4)).owner = blockNames[0];
						((MethodInsnNode)m.instructions.get(startIndex+6)).owner = blockNames[0];
						Util.info(++counter + ". Fluid block (" + target.cst + ") found");
					}
					
					if (target.cst.equals("flowing_lava") || target.cst.equals("lava"))
					{
						((TypeInsnNode)m.instructions.get(startIndex+1)).desc = blockNames[1];
						((MethodInsnNode)m.instructions.get(startIndex+4)).owner = blockNames[1];
						((MethodInsnNode)m.instructions.get(startIndex+6)).owner = blockNames[1];
						Util.info(++counter + ". Fluid block (" + target.cst + ") found");
					}
				}
			}
		}
		
		Util.info("Finite Fluids - Replaced " + counter + " blocks");
		Util.leaveSection();
		if (counter < 4) Util.error("WARNING! SOME BLOCKS MAY NOT HAVE BEEN PATCHED!");
		
		
	}
	
	
}
