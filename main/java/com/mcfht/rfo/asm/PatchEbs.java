package com.mcfht.rfo.asm;

import java.util.ArrayList;
import java.util.List;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;

import com.mcfht.rfo.asm.ASMTransformer.PatchTask;
import com.mcfht.rfo.utils.Util;

import net.minecraft.world.chunk.storage.ExtendedBlockStorage;

/**
 * <p>This class is not overly essential, might use it to make some methods synchronized.
 * 
 * 
 * @author FHT
 */
public class PatchEbs extends PatchTask
{
	
	/** Contains all the names of methods to make synchronized */
	public static final List<String> SynchronizedMethods = Util.makeList("func_150818_a", "a", "setExtBlockMetadata");


	/**
	 * Creates new EBS patcher. This will apply threadsafe additions to EBS objects.
	 * <p><i>Provide param of 0 to prevent patching.</i>
	 */
	public PatchEbs(int Attempts)
	{
		super(Attempts, NAMES, null);

	}
	
	/**
	 * Creates new EBS patcher. This will apply threadsafe additions to EBS objects.
	 * <p><i>Provide param of 0 to prevent patching.</i>
	 */
	public PatchEbs() 
	{
		this(-1);
	}

	/**
	 * Names of the classes that this method should target.
	 * <p><i>Used only within this class</i>
	 */
	public static String NAMES[] = 
	{
		"net.minecraft.world.chunk.storage.ExtendedBlockStorage",
		"apz"
	};

	/**
	 * {@link ExtendedBlockStorage#getBlockByExtId(int, int, int)} and
	 * {@link ExtendedBlockStorage#getExtBlockMetadata(int, int, int)}</b>
	 * <p>{@inheritDoc}}
	 */
	@Override
	@SuppressWarnings("unused")
	public void doPatch(String name, ClassNode c, boolean obf) 
	{
		for (MethodNode m : c.methods)
		{
			if (false && SynchronizedMethods.contains(m.name) && m.desc.contains("III"))
			{
				//m.access |= Opcodes.ACC_SYNCHRONIZED;
			}
		}
	}
}
