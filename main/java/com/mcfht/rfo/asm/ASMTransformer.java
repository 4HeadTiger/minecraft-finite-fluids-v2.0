package com.mcfht.rfo.asm;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;

import com.mcfht.rfo.RFOInfo;
import com.mcfht.rfo.utils.SpecialSet;

import net.minecraft.launchwrapper.IClassTransformer;
import net.minecraft.world.gen.MapGenBase;
import net.minecraft.world.gen.MapGenCaves;
import net.minecraft.world.gen.MapGenRavine;

/**
 * Exposes the Forge ASM interfaces for transforming vanilla bytecode.
 * @author FHT
 *
 */
public class ASMTransformer implements IClassTransformer
{


	protected static final int DEOBFUSCATED = 0x0;
	protected static final int OBFUSCATED = 0x1;
	
	/**
	 * This list contains all the patch tasks. To make a patch task, make a new class inheriting PathTasks,
	 * then use an instance of it to construct a new PatchTask and add it to the tasks list.
	 */
	public PatchTask[] tasks = 
	{
			//new PatchEbs(),
			new PatchDoors(0),
			new PatchDoors(1),
			new PatchBlockRegistry(),

			//TODO: Overwrite grass blocks
			
			//TODO: Replace cave and ravine generation instantiation, can use reflection though...
			//in net.minecraft.world.gen.ChunkProviderGenerate
			 
			  //    private MapGenBase caveGenerator = new MapGenCaves();    } Easy to redirect this part with reflection
			  //    private MapGenBase ravineGenerator = new MapGenRavine(); } 
	};
	
	/**
	 * Provides a parent class for the patch task stuff. To patch a vanilla class, extend this
	 * method and define custom transformations in the {@link PatchTask#doPatch(String, ClassNode, int)} 
	 * method.
	 * 
	 * @author FHT
	 *
	 */
	public static class PatchTask
	{

		/** The names {deobf, obf} of the class to be transformed by this task */
		public final String[] NAME = new String[2];
		
		public final boolean[] EXIT_CONDITION;
		
		/** The number of times to run this task. 1 for once, negative for infinite/always run, 0 for disable */
		public int attempts;

		/**
		 * Constructs a new patch task to attempt running the specified number of times. Negative indicates "always run"
		 * @param Attempts
		 */
		protected PatchTask(int Attempts, String[] names, boolean[] exitCondition)
		{
			attempts = Attempts;
			NAME[OBFUSCATED] = names[OBFUSCATED];
			NAME[DEOBFUSCATED] = names[DEOBFUSCATED];
			EXIT_CONDITION = exitCondition;
		}
		
		/**
		 * This method performs the desired patching behavior. Override this class in an inheriting
		 * object to define custom transformations.
		 * 
		 * <p>ClassNode is prepped for use. Changes are handled automatically at the end of thie method.
		 * 
		 * @param name => Name of this class as seen within the JVM.
		 * @param c => ClassNode of class to be transformed
		 * @param obf => Whether this ClassNode is obfuscated (1 = true, 0 = false).
		 * @return
		 */
		public void doPatch(String name, ClassNode c, boolean obf) {};
		
		/**
		 * Compares this patch task to the provided name.
		 * <i>Internal use only.</i>
		 * @param name
		 * @return
		 */
		public int get(String name)
		{
			if (this.NAME[OBFUSCATED].equals(name)) return OBFUSCATED;
			if (this.NAME[DEOBFUSCATED].equals(name)) return DEOBFUSCATED;
			return -1;
		}
	}
	
	
	/**
	 * <i>This method is used by Forge</i>. Transforms classes using tasks in the tasks list.
	 */
	@Override
	public byte[] transform(String name, String transformedName, byte[] cB) 
	{

		//System.out.println(" - Transforming " + name + " <=> " + name);
		//loop through all tasks
		for (int i = 0; i < tasks.length; i++)
		{
			if (tasks[i] == null) continue;
			if (tasks[i].EXIT_CONDITION != null && tasks[i].EXIT_CONDITION[0] == false) continue;

			//exit early.
			
			//System.out.println(" = task: " + tasks[i].getClass().getSimpleName());
			
			int j;
			//Clear dead tasks
			if (tasks[i].attempts == 0) tasks[i] = null;
			//we have a match with remaining attempts
			//else 
			if (tasks[i] != null && ((j = tasks[i].get(transformedName)) >= 0 || (j = tasks[i].get(name)) >= 0))
			{
				//create a new classnode and load this class into it;
				ClassNode c = new ClassNode();
				ClassReader r = new ClassReader(cB);
				r.accept(c, 0);
				
				//patch it
				tasks[i].doPatch(transformedName, c, j == OBFUSCATED);
				
				//now send any changes back into the classbytes array
				ClassWriter w = new ClassWriter(ClassWriter.COMPUTE_FRAMES /* | ClassWriter.COMPUTE_MAXS*/ );
				
				c.accept(w);
				cB = w.toByteArray();
				
				//now reduce the attempts tickCounter
				tasks[i].attempts -= 1;
				
				return cB;
			}
		}
		
		
		return cB;
	}
	

}
