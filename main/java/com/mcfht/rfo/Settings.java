package com.mcfht.rfo;

import java.io.File;

import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.compatibility.ATGRiverPatch;
import com.mcfht.rfo.compatibility.BCFluids;
import com.mcfht.rfo.utils.Util;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.common.config.Property.Type;

public class Settings 
{
	
	
	/** The amount of fluid in a full block */
	public static final int FLUID_MAX = 8192;

	/** The highest possible pressure value. Currently pressure is stored in a short, so this should be enough. */
	public static final int PRESSURE_MAX = 8192;

	/** The amount of pressure applied downwards by a complete block. Allows roughly PRESSURE_MAX / PRESSURE_STEP blocks of propagation*/
	public static final int PRESSURE_STEP = 16;
	
	/** The amount of fluid per metadata 'layer' of a fluid block */
	public static final int FLUID_LAYER = FLUID_MAX >> 3;
	
	/** Viscosity of water blocks. See: {@link BlockFiniteFluid#computeMinBlock(float)} and
	 * {@link BlockFiniteFluid#computeTickRate(float)}*/
	public static final float WATER_VISC = 1.0F;
	
	/** density of water blocks */
	public static final int WATER_DENSITY = 1000;

	/** Viscosity of lava blocks. Real lava is much more viscous, but this is a nice balance */
	public static final float LAVA_VISC = 5.0F;
	
	/** Viscosity of lava blocks in the nether */
	public static final float LAVA_VISC_NETHER = 2.5F;
	
	/** density of lava blocks */
	public static final int LAVA_DENSITY = 3000;
	
	//////////////////////// CONFIGURIZATIONABLE SETTINGS /////////////////////////////////
	
	/** Whether to perform processing calculations this thread */
	public static boolean FLOW_ENABLED = true;
	
	/** Whether pressure is enabled or disabled. Disable to improve performance. May break other features. */
	public static boolean PRESSURE_ENABLED = true;
	
	/** The number of threads to use for various tasks [not yet determined all stuff] */
	public static int UPDATER_COUNT = 1;

	/** The current nuber of ticks between each block update */
	public static int TICK_CURRENT = 5;
	
	/** The range in which to perform near updates*/
	public static int RANGE_NEAR_ABS = 3;
	/** The euclidian representation of {@linkplain #RANGE_NEAR_ABS}*/
	public static int RANGE_NEAR = RANGE_NEAR_ABS * RANGE_NEAR_ABS;
	
	/** The range in which to perform far updates */
	public static int RANGE_FAR_ABS = 10;
	/** The euclidian representation of {@linkplain #RANGE_FAR_ABS}*/
	public static int RANGE_FAR = RANGE_FAR_ABS * RANGE_FAR_ABS;
	
	/** The load factor at which point to cease doing far updates. */
	public static float LOAD_FACTOR = 0.6F;
	
	/** A multiplier, the number of ms represented by load factor per tick. */
	public static int LOAD_MULT = (int) (50.F * LOAD_FACTOR);

	/** Whether to enable 'natural' equalization. */
	public static boolean EQUALIZE_NATURAL = true;
	
	/** The fraction of a block required to perform rounded equalization*/
	
	public static float EQUALIZE_NATURAL_TENDENCY = 1F;
	
	/** The minimum content of a block required to perform rounded equalization */
	public static int EQUALIZE_NATURAL_MIN_BLOCK = (int) ((float)Settings.FLUID_MAX * EQUALIZE_NATURAL_TENDENCY) ;
	
	/** Some random thing. Might not need or use this anywhere */
	public static int EQUALIZE_NAT_SHALLOW_CHANCE = (0x1 << (int)(4F * EQUALIZE_NATURAL_TENDENCY)) - 1;

	/** Whether to save and load fluid levels from NBT (strongly recommended) */
	public static boolean SAVE_NBT = true;
	
	/** Some random number indicating the rate at which distant updates should be performed. Higher values mean more frequent updates. */
	public static int UPDATE_REDUCTION_FALLOFF = 2;
	
	
	//Arrays are technically just object references, although Java somewhat obscures this fact.
	/** Whether to patch packets. Uses array reference, so changes can be seen by lots of stuff */
	/*UNUSED*/ public static boolean[] PATCH_PACKETS = { false }; /*UNUSED*/
	
	/** Whether to patch doors. Uses array reference, so changes can be seen by lots of stuff */
	public static boolean[] PATCH_DOORS = { true };

	
	
	public static final String[] category = 
		{
			"1. GENERAL SETTINGS",
			"2. EQUALIZER",
			"3. CORE MODDING",
			"4. COMPATIBILITY"
		};
	
	
	/** The file leading to the config for this instance. */
	public static File configPath;
	
	/** Gets/Writes all relevant settings into the config file. */
	public static void handleConfigs()
	{
		Configuration config = new Configuration(configPath);
		config.load();
		
		///////////////// GENERAL ///////////////////
		//bools
		FLOW_ENABLED = 					config.getBoolean("1. Enable Fluid Processing", category[0], FLOW_ENABLED, "Whether fluids should be processed. Disabling will cause all fluids not to move." );
		
		PRESSURE_ENABLED = 				config.getBoolean("3. Enable Pressure", category[0], PRESSURE_ENABLED, "Whether to calculate fluid pressure. Disabling may improve performance slightly." );
		
		SAVE_NBT = 						config.getBoolean("9. Save to NBT", category[0], SAVE_NBT, "Whether to save fluid levels to NBT. Strongly recommended, but not essential.");
		
		//ints
		TICK_CURRENT = 					config.getInt("2. Ideal update rate", category[0], TICK_CURRENT, 2, 999, "Number of ticks between flow updates. Lower values increase flow speed." );
		
		RANGE_NEAR_ABS = 				config.getInt("4. Range for high priority updates", category[0], RANGE_NEAR_ABS, 1, 20, "Chunks within this euclidian distance, will always be updated. " );
		
		RANGE_FAR_ABS = 				config.getInt("5. Range for low priority updates", category[0], RANGE_FAR_ABS, 1, 30, "Chunks within this far range may not be updated if the server is under load");
		
		UPDATER_COUNT = 				config.getInt("7. Number of Flow Updaters", category[0], UPDATER_COUNT, 1, 16, "The number threads to use for flow. See: Readme for info on how to choose the best number of threads." );
		
		UPDATE_REDUCTION_FALLOFF = 		config.getInt("8. Distance scale for chunk update frequency", category[0], UPDATE_REDUCTION_FALLOFF, 1, 10, "Higher values increase frequency of distant client<=>server chunk updates. 2-4 recommended." );
		
		//floats
		LOAD_FACTOR = 					config.getFloat("6. Load Factor for far updates", category[0], LOAD_FACTOR, 0.1F, 1.0F, "The load factor at which point to cease far updates. Lower values can improve performance");

		///////////////// EQUALIZATION ///////////////////
		EQUALIZE_NATURAL = 				config.getBoolean("1. Natural Equalization", category[1], EQUALIZE_NATURAL, "Whether to equalize large bodies of fluid naturally.");
		
		EQUALIZE_NATURAL_TENDENCY = 	config.getFloat("2. Natural Equilibrium Factor", category[2], EQUALIZE_NATURAL_TENDENCY, 0.01F, 1.0F, "The required fluid content of a block for natural equlibrium to occur.");

		
		///////////////// CORE MODDING ///////////////////
		PATCH_DOORS[0] = 				config.getBoolean("1. Patch doors", category[2], PATCH_DOORS[0], "Makes doors and trapdoors throw block updates (aka notify blocks when they open and close)." );
		

		///////////////// COMPATIBILITY SETTINGS ///////////////////
		ATGRiverPatch.INSTALLED = 	config.getBoolean("1. Prevent ATG Rivers", category[3], ATGRiverPatch.INSTALLED, "Block ATG rivers from spawning by default.");
		
		BCFluids.PATCH_BC = 			config.getBoolean("2. Patch Buildcraft fluids", category[3], BCFluids.PATCH_BC, "Whether to replace Buildcraft oil and fuel with finite variations. Turning this off in existing world is not recommended.");


		
		recomputeOtherConstants();
		config.save();
	}
	
	public static void resetConfig()
	{
		//erase the old config, allows us to flush all the settings.
		if (configPath.delete())
		{
			Util.info(" - Updating settings changes in config (" + configPath.getAbsolutePath() + ")");
			handleConfigs();
		}
	}
	
	/** Recomputes some values that may stem from the displayed settings. */
	public static void recomputeOtherConstants()
	{
		//recompute some equalization settings
		EQUALIZE_NATURAL_MIN_BLOCK = (int) ((float)Settings.FLUID_MAX * EQUALIZE_NATURAL_TENDENCY) ;
		EQUALIZE_NAT_SHALLOW_CHANCE = (0x1 << (int)(4F * EQUALIZE_NATURAL_TENDENCY)) - 1;		
		
		//compute the values we will actually use in game
		RANGE_NEAR = RANGE_NEAR_ABS * RANGE_NEAR_ABS;
		RANGE_FAR = RANGE_FAR_ABS * RANGE_FAR_ABS;
		LOAD_MULT = (int) (50.F * LOAD_FACTOR);
		
	}
	
	
}
