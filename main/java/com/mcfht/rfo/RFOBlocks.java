package com.mcfht.rfo;

import com.mcfht.rfo.blocks.BlockCreativeSource;
import com.mcfht.rfo.blocks.BlockPumpBase;
import com.mcfht.rfo.blocks.PressureProvider;
import com.mcfht.rfo.tileentities.TileEntityPump;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fluids.FluidRegistry;

/**
 * A simple class to handle the creation and registration of the blocks etc in the mod.
 * 
 * @author FHT
 *
 */
public class RFOBlocks 
{
	/** This block is magic. */
    public static Block creativeSource;
    
    /** Provides pressure to neighboring blocks. It is very badass. */
    public static Block testProvider;

    /** Provides pressure to neighboring blocks. It is very badass. */
    public static Block basicPump;
	    
    
    public static void init()
    {
    	
    	//register tile entities
    	
		GameRegistry.registerTileEntity(TileEntityPump.class, "fftePump");

    	
	    //Creative source
		creativeSource = new BlockCreativeSource(Material.rock).setBlockName("ffCreative").setCreativeTab(CreativeTabs.tabMisc);
		LanguageRegistry.addName(creativeSource, "Infinite Source");
		GameRegistry.registerBlock(creativeSource, "creativeSourceB");
	
		//testProvider = new PressureProvider(Material.rock, Settings.PRESSURE_MAX).setBlockName("ffProvider").setCreativeTab(CreativeTabs.tabMisc);
		//LanguageRegistry.addName(testProvider, "Pressure Provider Placeholder");
		//GameRegistry.registerBlock(testProvider, "testProviderB");
		
		basicPump = new BlockPumpBase(Material.rock, Settings.PRESSURE_STEP * 5, 0).setBlockName("ffPumpbasic").setCreativeTab(CreativeTabs.tabMisc);
		LanguageRegistry.addName(basicPump, "Basic Pump");
		GameRegistry.registerBlock(basicPump, "pump_basic");
    }
	
} 
