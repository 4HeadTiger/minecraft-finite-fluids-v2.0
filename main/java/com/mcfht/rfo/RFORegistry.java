package com.mcfht.rfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.blocks.interactions.Interaction;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

/**
 * Contains all of the finite fluid blocks in this instance. Provides IDs etcetera. Primarily used
 * to provide ID's to fluids.
 * @author FHT
 *
 */
public class RFORegistry 
{

	/** The number of fluids registered. Should only be incremented in one place ever*/
	private static int fluidCount = 0;
	public static int getRegisteredFluidCount() { return fluidCount; }

	/** Links FFluids to names. Internal use only. */
	private static Map<String, FFluid> fluids = new HashMap<String, FFluid>();
	/** Links FFluids to ID's. Internal use only. */
	private static Map<Integer, FFluid> fluidIDs = new HashMap<Integer, FFluid>();

    
	
	/**
	 * Registers a fluid interaction for this finite fluid with some other fluid type
	 * @param name
	 * @param target
	 * @param ii
	 */
	public static void registerFluidInteraction(String name, String target, Interaction ii) 
	{
		//first find the fluid
		FFluid f0 = fluids.get(name);
		if (f0 == null) return;
		
		Fluid f1 = FluidRegistry.getFluid(target);
		if (f1 == null) 
		{
			f1 = new Fluid(target);
			FluidRegistry.registerFluid(f1);
		}
		
		//now register for every block
		for (BlockFiniteFluid b : f0.blocks)
			b.registerInteraction(f1, ii);
	}
	
	/**
	 * Sets the block information for the fluid associated with this name.
	 * @param name
	 * @param full
	 * @param partial
	 * @param empty
	 */
	public static void setBlockFluidInformation(FFluid fluid) 
	{
		Iterator<BlockFiniteFluid> it = fluid.blocks.iterator();
		while (it.hasNext())
		{
			BlockFiniteFluid b = it.next();
			if (b != fluid.get(0) && b != fluid.get(1) && b != fluid.empty) it.remove();
			b.ffluid = fluid;
		}
	}
	
	/** Gets the Finite Fluid associated with this name */
	public static FFluid getFFluidByName(String name)
	{
		return fluids.get(name);
	}
	
	/** Gets the Finite Fluid associated with this name */
	public static FFluid getFFluidByID(int id)
	{
		return fluids.get(id);
	}
	
	/** Retrieves the first registered variant (usually the "full" variant) of the Finite Fluid
	 * registered to this Forge Fluid. Returns null if no FF is registered. */
	public static BlockFiniteFluid getFromFluid(Fluid f)
	{
		FFluid ff = fluids.get(f.getName());
		if (ff == null || ff.blocks == null) return null;
		return ff.blocks.get(0);
	}
	
	/**
	 * Registers a new finite fluid, then returns the block that we just created. Registers the blocks in block registry etc.
	 * 
	 * <p>Most of the time, this method will be enough. 
	 * For more control, use {@link #registerFluid(String, BlockFiniteFluid, BlockFiniteFluid, Block)}.
	 * 
	 * <p>For more info on parameters, see {@link BlockFiniteFluid#BlockFiniteFluid(FFluid, Material, float, int, String[])}
	 * 
	 * @param name The name of the fluid type (i.e "water", "lava", "oil", etc)
	 * @param mat The material for this fluid
	 * @param visc The viscosity for this fluid. Defines flow characteristics.
	 * @param density Relative weight of the fluid. Heavier fluids sink. Should roughly use kg per 1000L (aka a full block).
	 * @param tex A collection which contains {texture_still, texture_flowing} as strings to the texture domain.
	 * @return The new finite fluid as an FFluid.
	 */
	public static FFluid registerFluid(String name, Material mat, float visc, int density, String[] tex)
	{
		FFluid ff = fluids.get(name);
		if (ff == null) ff = new FFluid(name, mat);
		
		if (ff.blocks.size() > 0) 
			throw new RuntimeException("A Finite Fluid is already registered for fluid:" + name, new Throwable("Fluid already registered"));
		
		BlockFiniteFluid bFull = new BlockFiniteFluid(ff, mat, visc, density, tex);
		BlockFiniteFluid bPartial = new BlockFiniteFluid(ff, mat, visc, density, tex);

		//register both blocks in the block lists and game registry
		GameRegistry.registerBlock(bFull, "rfo_" + ff.name + "_" + ff.blocks.size());
		ff.blocks.add(bFull);
		
		GameRegistry.registerBlock(bPartial, "rfo_" + ff.name + "_" + ff.blocks.size());
		ff.blocks.add(bPartial);
		
		//Sets the info for the block
		setBlockFluidInformation(ff); 

		//and set the oil block here
		FluidRegistry.getFluid(name).setBlock(bFull);

		//and register for them some generic names?
		//LanguageRegistry.addName(bFull, ff.name.substring(0,1).toUpperCase() + ff.name.substring(1) + " Full");
		//LanguageRegistry.addName(bPartial, ff.name.substring(0,1).toUpperCase() + ff.name.substring(1) + " Partial");

		return ff;
	}

	
	/** Registers two pre-initialized fluid blocks. See {@link BlockFiniteFluid#BlockFiniteFluid(FFluid, Material, float, int, String[])}.
	 * 
	 * <p>It is assumed that these blocks have been registered in the blockregistry etc. This method merely registers them into
	 * other systems within the mod. Useful if you want to use a custom class to define additional fluid behavior (i.e, Lava flowing
	 * differently in Nether and Overworld).
	 */
	public static FFluid registerFluid(String name, BlockFiniteFluid bFull, BlockFiniteFluid bPartial, Block empty)
	{
		FFluid ff = fluids.get(name);
		if (ff == null) ff = new FFluid(name, bFull.getMaterial(), empty == null ? Blocks.air : empty);
			
		if (ff.blocks.size() > 0) 
			throw new RuntimeException("A Finite Fluid is already registered for fluid:" + name, new Throwable("Fluid already registered"));

		//list them into the FFluid
		ff.blocks.add(bFull);
		ff.blocks.add(bPartial);

		setBlockFluidInformation(ff); 

		return ff;
	}


	/**
	 * This class describes any realistic fluid in this mod. This class contains block variants for the fluid, along
	 * with some other information - the ID of the fluid, the MC Fluid it describes, etc.
	 */
	public static class FFluid
	{
		public final String name;
		public final Fluid fluid;
		public final int ID;
		private final List<BlockFiniteFluid> blocks = new ArrayList<BlockFiniteFluid>();
		public final Material mat;
		public final Block empty;
		public boolean isFlammable = false;
		
		public int flammability, encourage;
		
		public static enum State
		{
			FULL,
			PARTIAL;
		}
		
		//forge people can't spell flammability
		public FFluid setFireStats(int flammibality, int encouragement)
		{
		
			flammability = flammibality;
			encourage = encouragement;
			
			isFlammable = flammability > 0;
			Blocks.fire.setFireInfo(blocks.get(0), encouragement, flammibality);
			Blocks.fire.setFireInfo(blocks.get(1), encouragement, flammibality);
			return this;
		}
		
		/** Gets the finite fluid block associated with this spot. 0 = full, 1 = partial. */
		public BlockFiniteFluid get(int val) { return blocks.get(val); }
		
		/** Gets the finite fluid block associated with this state (full or partial). */
		public BlockFiniteFluid get(State s) { return blocks.get(s == State.FULL ? 0 : 1); }
		
		/** Internal use only. Creates and registers a new FFluid. Creates and registers a new ForgeFluid if appropriate. */
		private FFluid(String name, Material m) { this(name, m, Blocks.air); }


		/** Internal use only. Creates and registers a new FFluid. Creates and registers a new ForgeFluid if appropriate. */
		private FFluid(String Name, Material m, Block Empty)
		{
			FFluid f0 = fluids.get(Name);
			if (f0 == null)
			{			
				mat = m;
				name = Name;
				ID = ++fluidCount;
				empty = Empty;
				if (FluidRegistry.getFluid(Name) == null) FluidRegistry.registerFluid(new Fluid(Name));
				
				fluid = FluidRegistry.getFluid(Name);
				fluids.put(Name, this);
				fluidIDs.put(ID, this);
			}
			else
			{
				//this instance will die soon, but we need to initialize final fields or the compiler
				//will have a seizure.
				name = "default";
				ID = -1;
				mat = null;
				fluid = null;
				empty = null;
			}
		}
	}
	
	
	
	
	
	
	
}
