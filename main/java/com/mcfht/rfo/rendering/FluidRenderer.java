package com.mcfht.rfo.rendering;

import com.mcfht.rfo.blocks.BlockFiniteFluid;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;

public class FluidRenderer implements ISimpleBlockRenderingHandler {

	public static final int RENDERID = 2395827;
	
	@Override
	public void renderInventoryBlock(Block block, int metadata, int modelId, RenderBlocks renderer) {
		//pretty sure it will probably be fine without this... I think...
	}

	@Override
	public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks r) 
	{
		Block b = world.getBlock(x, y, z);
        Tessellator tessellator = Tessellator.instance;
        
        int colBits = b.colorMultiplier(world, x, y, z);
        float Rmult = (float)(colBits >> 16 & 255) / 255.0F;
        float Gmult = (float)(colBits >> 8 & 255) / 255.0F;
        float Bmult = (float)(colBits & 255) / 255.0F;
        
        boolean renderTopFace = b.shouldSideBeRendered(world, x, y + 1, z, 1);
        boolean renderBottomFace = b.shouldSideBeRendered(world, x, y - 1, z, 0);
        boolean[] renderSide = new boolean[] {b.shouldSideBeRendered(world, x, y, z - 1, 2), b.shouldSideBeRendered(world, x, y, z + 1, 3), b.shouldSideBeRendered(world, x - 1, y, z, 4), b.shouldSideBeRendered(world, x + 1, y, z, 5)};

        if (!renderTopFace && !renderSide[0] && !renderSide[1] && !renderSide[2] && !renderSide[3] && !renderBottomFace )
        {
            return false;
        }
        else
        {
            boolean didRender = false;
            
            
            float f3 = 0.5F; //0.5
            float f4 = 1.0F; //1.0
            float f5 = 0.8F; //0.8
            float f6 = 0.6F; //0.6
            double d0 = 0.0D; //0.0
            double d1 = 1.0D; //1.0
            Material material = b.getMaterial();
            int m0 = world.getBlockMetadata(x, y, z);
            
            double height0 = (double)getLiquidHeight(r, b, x, y, z, material);
            double height1 = (double)getLiquidHeight(r, b, x, y, z + 1, material);
            double height2 = (double)getLiquidHeight(r, b, x + 1, y, z + 1, material);
            double height3 = (double)getLiquidHeight(r, b, x + 1, y, z, material);
            
            double d6 = 0.001D; //extra crap: 0000000474974513D;
            
            float f9;
            float f10;
            float f11;

            if (r.renderAllFaces || renderTopFace)
            {
                didRender = true;
                IIcon texTop = r.getBlockIconFromSideAndMetadata(b, 1, m0);
                float flowDirection = (float)BlockLiquid.getFlowDirection(world, x, y, z, material);

                if (flowDirection > -999.0F)
                {
                    texTop = r.getBlockIconFromSideAndMetadata(b, 2, m0);
                }

                height0 -= d6;
                height1 -= d6;
                height2 -= d6;
                height3 -= d6;
                double d7;
                double d8;
                double d10;
                double d12;
                double d14;
                double d16;
                double d18;
                double d20;

                if (flowDirection < -999.0F)
                {
                    d7 = (double)texTop.getInterpolatedU(0.0D);
                    d14 = (double)texTop.getInterpolatedV(0.0D);
                    d8 = d7;
                    d16 = (double)texTop.getInterpolatedV(16.0D);
                    d10 = (double)texTop.getInterpolatedU(16.0D);
                    d18 = d16;
                    d12 = d10;
                    d20 = d14;
                }
                else
                {
                    f9 = MathHelper.sin(flowDirection) * 0.25F;
                    f10 = MathHelper.cos(flowDirection) * 0.25F;
                    f11 = 8.0F;
                    d7 = (double)texTop.getInterpolatedU((double)(8.0F + (-f10 - f9) * 16.0F));
                    d14 = (double)texTop.getInterpolatedV((double)(8.0F + (-f10 + f9) * 16.0F));
                    d8 = (double)texTop.getInterpolatedU((double)(8.0F + (-f10 + f9) * 16.0F));
                    d16 = (double)texTop.getInterpolatedV((double)(8.0F + (f10 + f9) * 16.0F));
                    d10 = (double)texTop.getInterpolatedU((double)(8.0F + (f10 + f9) * 16.0F));
                    d18 = (double)texTop.getInterpolatedV((double)(8.0F + (f10 - f9) * 16.0F));
                    d12 = (double)texTop.getInterpolatedU((double)(8.0F + (f10 - f9) * 16.0F));
                    d20 = (double)texTop.getInterpolatedV((double)(8.0F + (-f10 - f9) * 16.0F));
                }

                tessellator.setBrightness(b.getMixedBrightnessForBlock(world, x, y, z));
                tessellator.setColorOpaque_F(f4 * Rmult, f4 * Gmult, f4 * Bmult);
                tessellator.addVertexWithUV((double)(x + 0), (double)y + height0, (double)(z + 0), d7, d14);
                tessellator.addVertexWithUV((double)(x + 0), (double)y + height1, (double)(z + 1), d8, d16);
                tessellator.addVertexWithUV((double)(x + 1), (double)y + height2, (double)(z + 1), d10, d18);
                tessellator.addVertexWithUV((double)(x + 1), (double)y + height3, (double)(z + 0), d12, d20);
                tessellator.addVertexWithUV((double)(x + 0), (double)y + height0, (double)(z + 0), d7, d14);
                tessellator.addVertexWithUV((double)(x + 1), (double)y + height3, (double)(z + 0), d12, d20);
                tessellator.addVertexWithUV((double)(x + 1), (double)y + height2, (double)(z + 1), d10, d18);
                tessellator.addVertexWithUV((double)(x + 0), (double)y + height1, (double)(z + 1), d8, d16);
            }

            if (r.renderAllFaces || renderBottomFace)
            {
                tessellator.setBrightness(b.getMixedBrightnessForBlock(world, x, y - 1, z));
                tessellator.setColorOpaque_F(f3, f3, f3);
                r.renderFaceYNeg(b, (double)x, (double)y + d6, (double)z, r.getBlockIconFromSide(b, 0));
                didRender = true;
            }

            for (int k1 = 0; k1 < 4; ++k1)
            {
                int l1 = x;
                int j1 = z;

                if (k1 == 0)
                {
                    j1 = z - 1;
                }

                if (k1 == 1)
                {
                    ++j1;
                }

                if (k1 == 2)
                {
                    l1 = x - 1;
                }

                if (k1 == 3)
                {
                    ++l1;
                }

                IIcon texSide = r.getBlockIconFromSideAndMetadata(b, k1 + 2, m0);

                if (r.renderAllFaces || renderSide[k1])
                {
                    double d9;
                    double d11;
                    double d13;
                    double d15;
                    double d17;
                    double d19;

                    if (k1 == 0)
                    {
                        d9 = height0;
                        d11 = height3;
                        d13 = (double)x;
                        d17 = (double)(x + 1);
                        d15 = (double)z + d6;
                        d19 = (double)z + d6;
                    }
                    else if (k1 == 1)
                    {
                        d9 = height2;
                        d11 = height1;
                        d13 = (double)(x + 1);
                        d17 = (double)x;
                        d15 = (double)(z + 1) - d6;
                        d19 = (double)(z + 1) - d6;
                    }
                    else if (k1 == 2)
                    {
                        d9 = height1;
                        d11 = height0;
                        d13 = (double)x + d6;
                        d17 = (double)x + d6;
                        d15 = (double)(z + 1);
                        d19 = (double)z;
                    }
                    else
                    {
                        d9 = height3;
                        d11 = height2;
                        d13 = (double)(x + 1) - d6;
                        d17 = (double)(x + 1) - d6;
                        d15 = (double)z;
                        d19 = (double)(z + 1);
                    }

                    didRender = true;
                    float f8 = texSide.getInterpolatedU(0.0D);
                    f9 = texSide.getInterpolatedU(8.0D);
                    f10 = texSide.getInterpolatedV((1.0D - d9) * 16.0D * 0.5D);
                    f11 = texSide.getInterpolatedV((1.0D - d11) * 16.0D * 0.5D);
                    float f12 = texSide.getInterpolatedV(8.0D);
                    tessellator.setBrightness(b.getMixedBrightnessForBlock(world, l1, y, j1));
                    float f13 = 1.0F;
                    f13 *= k1 < 2 ? f5 : f6;
                    tessellator.setColorOpaque_F(f4 * f13 * Rmult, f4 * f13 * Gmult, f4 * f13 * Bmult);
                    tessellator.addVertexWithUV(d13, (double)y + d9, d15, (double)f8, (double)f10);
                    tessellator.addVertexWithUV(d17, (double)y + d11, d19, (double)f9, (double)f11);
                    tessellator.addVertexWithUV(d17, (double)(y + 0), d19, (double)f9, (double)f12);
                    tessellator.addVertexWithUV(d13, (double)(y + 0), d15, (double)f8, (double)f12);
                    tessellator.addVertexWithUV(d13, (double)(y + 0), d15, (double)f8, (double)f12);
                    tessellator.addVertexWithUV(d17, (double)(y + 0), d19, (double)f9, (double)f12);
                    tessellator.addVertexWithUV(d17, (double)y + d11, d19, (double)f9, (double)f11);
                    tessellator.addVertexWithUV(d13, (double)y + d9, d15, (double)f8, (double)f10);
                }
            }

            r.renderMinY = d0;
            r.renderMaxY = d1;
            return didRender;
        }
	}

	@Override
	public boolean shouldRender3DInInventory(int modelId) {
		//does this need to do anything?
		return false;
	}

	@Override
	public int getRenderId() {
		//pretty sure I can use bigass numbers like this
		return RENDERID;
	}
	


	/**
	 * Returns the liquid height from surrounding blocks, this part is fairly easy.
	 * @param r
	 * @param x
	 * @param y
	 * @param z
	 * @param mat
	 * @return
	 */
	public static float getLiquidHeight(RenderBlocks r, Block bN, int x, int y, int z, Material mat)
    {
		int l = 0;
        float f = 0.0F;
        
        boolean isFromFF = bN instanceof BlockFiniteFluid;
        
        for (int i = 0; i < 4; ++i)
        {
            int x1 = x - (i & 1);
            int z1 = z - (i >> 1 & 1);

            Block b1 = r.blockAccess.getBlock(x1, y + 1, z1);

            boolean isComparable  = isFromFF 
            		&& (b1 instanceof BlockFiniteFluid) 
            		&& ((BlockFiniteFluid)b1).getFluid() == ((BlockFiniteFluid)bN).getFluid();
            
            if (isComparable || (isFromFF == false && b1.getMaterial() == mat))
            {
                return 1.0F;
            }

            
            b1 = r.blockAccess.getBlock(x1, y, z1);
            
            if (b1.getMaterial() == mat)
            {
                int l1 = r.blockAccess.getBlockMetadata(x1, y, z1);

                if (l1 >= 8 || l1 == 0)
                {
                    f += BlockLiquid.getLiquidHeightPercent(l1) * 10.0F;
                    l += 10;
                }

                f += BlockLiquid.getLiquidHeightPercent(l1);
                ++l;
            }
            else if (!b1.getMaterial().isSolid())
            {
                ++f;
                ++l;
            }
        }

        return 1.0F - f / (float)l;
    }

	
	

}
