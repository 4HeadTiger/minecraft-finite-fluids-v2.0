package com.mcfht.rfo.blocks;

import com.mcfht.rfo.RFORegistry;
import com.mcfht.rfo.Settings;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.world.World;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

public class BlockFiniteLava extends BlockFiniteFluid
{
	/** The minimum block content in the nether */
	public final int minBlockNether;
	/** The rate of flow in the nether */
	public final int tickRateNether;
	
	public BlockFiniteLava(Material mat) 
	{
		super(null, Material.lava, Settings.LAVA_VISC, Settings.LAVA_DENSITY, new String[]{"lava_still", "lava_flow"});

		//compute the values we will use in the nether
		tickRateNether = computeTickRate(Settings.LAVA_VISC_NETHER);
		minBlockNether = computeMinBlock(Settings.LAVA_VISC_NETHER);
	}

	//@Override public Block getBlockFull() { return Blocks.lava; }
	//@Override public Block getBlockPartial() { return Blocks.flowing_lava; }
	//@Override public Fluid getFluid() { return FluidRegistry.LAVA; }
	
	@Override public int getTickRate(World w) { return w.provider.dimensionId == -1 ? tickRateNether : tickRate; }
	@Override public int getMinBlock(World w)  {  return (int) (w.provider.dimensionId == -1 ? minBlockNether : minBlock);  }
	
	/** The item representing a bucket of this block */
	@Override public Item getBucket() { return Items.lava_bucket; }
	

	@Override public int doBreak(IChunk d1, Block b1, int x1, int y1, int z1, int m1) 
	{
		d1.getIWorld().setBlock(d1, x1, y1, z1, b1, m1, 0x7);
		//TODO make it play a sound. Idk if I can do this from a thread.
		return 1;
	}
	
	@Override public int getEquilibriumRequirement(World w, Block bB)
	{
		return w.provider.dimensionId == -1 ? super.getEquilibriumRequirement(w, bB) : Integer.MAX_VALUE;
	}
	
    @SideOnly(Side.CLIENT) public boolean getCanBlockGrass() { return this.canBlockGrass; }

    
	 /*
     * The game crashes if some of these methods are not present. Some should be inherited but for some reason are not,
     */
	
    /////////////////////////// METHOD STUBS FOR COMPAT AND STABILITY ///////////////////////////
    
	//Cbf figuring out which ones are needed and which ones are not needed. This should be fine though;
    
    @Override public boolean func_149698_L() { return true; }
    @Override public Block setHardness(float f){ return super.setHardness(f);  }
    public Block c(float f) { this.blockHardness = f; return this; }
    @Override public Block setLightOpacity(int o) { return super.setLightOpacity(0); }
    @Override public Block setBlockName(String name) { return super.setBlockName(name);  }
    @Override public Block setLightLevel(float f) { return super.setLightLevel(f); }
    @Override public Block setBlockTextureName(String tex) { return super.setBlockTextureName(tex); }
    @Override public Block disableStats() { return super.disableStats(); } 
    
    
    //The following fields and methods are contained within BlockLiquid and BlockLiquidDynamic. Cbf figuring out
    //exactly which ones need to be here and which ones do not.
    
    int field_149815_a;
    boolean[] field_149814_b = new boolean[4];
    int[] field_149816_M = new int[4];
    private void func_149811_n(World p_149811_1_, int p_149811_2_, int p_149811_3_, int p_149811_4_) { return;  }
    private void func_149813_h(World p_149813_1_, int p_149813_2_, int p_149813_3_, int p_149813_4_, int p_149813_5_) { return; }
    private int func_149812_c(World p_149812_1_, int p_149812_2_, int p_149812_3_, int p_149812_4_, int p_149812_5_, int p_149812_6_) { return 0; }
    private boolean[] func_149808_o(World p_149808_1_, int p_149808_2_, int p_149808_3_, int p_149808_4_) { return null; }
    private boolean func_149807_p(World p_149807_1_, int p_149807_2_, int p_149807_3_, int p_149807_4_){ return false; }
    protected int func_149810_a(World p_149810_1_, int p_149810_2_, int p_149810_3_, int p_149810_4_, int p_149810_5_) { return 0; }
    private boolean func_149809_q(World p_149809_1_, int p_149809_2_, int p_149809_3_, int p_149809_4_) { return false; }
    
	
}
