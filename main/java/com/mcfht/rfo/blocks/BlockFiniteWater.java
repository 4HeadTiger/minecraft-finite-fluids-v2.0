package com.mcfht.rfo.blocks;

import java.util.Random;

import com.mcfht.rfo.RFORegistry;
import com.mcfht.rfo.Settings;
import com.mcfht.rfo.compatibility.GlennsGases;
import com.mcfht.rfo.RFORegistry.FFluid;
import com.mcfht.rfo.datastructure.Manager;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.datastructure.DataInterfaces.IWorld;
import com.mcfht.rfo.scheduler.deferred.CreateSteam;
import com.mcfht.rfo.utils.Util;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;

public class BlockFiniteWater extends BlockFiniteFluid
{


	
	
	public BlockFiniteWater(Material mat) 
	{
		super(null, Material.water, Settings.WATER_VISC, Settings.WATER_DENSITY, new String[]{"water_still", "water_flow"});
	}

	/** Should return block to use when this fluid becomes full */
	//@Override public Block getBlockFull() { return Blocks.water; }
	
	/** should return the block to use when this fluid is placed as partially filled */
	//@Override public Block getBlockPartial() { return Blocks.flowing_water; }
	
	@Override
	public void updateTick(World w, int x, int y, int z, Random rand)
	{
		super.updateTick(w, x, y, z, rand);
		if (w.isRemote || !GlennsGases.INSTALLED) return;
		
		int evapCount = 0;
		
		//evaporate from lava and make steam, when glenn's gases is installed
		for (int xx = x - 4; xx <= x + 4; xx++)
		{
			for (int zz = z - 4; zz <= z + 4; zz++)
			{
				for (int yy = y - 4; yy <= y + 1; yy++)
				{
					Block b = w.getBlock(xx, yy, zz);
					if (b == Blocks.lava || b == Blocks.flowing_lava)
					{
						evapCount += 10;
					}
					else if (b == Blocks.fire)
					{
						evapCount += 4;
					}
				}
			}
		}
		
		if (evapCount > Util.rand.nextInt(120))
		{
			IWorld W = Manager.instance.getOrRegisterWorld(w);
			IChunk d0 = W.getOrRegisterChunk(w.getChunkFromChunkCoords(x >> 4, z >> 4));
		
			Block bA = w.getBlock(x, y+1, z);
			int ii = 1;
			for (ii = 1; bA != Blocks.air && ii < 4 && y + ii < 255; ii++)
			{
				bA = w.getBlock(x, y + ii, z);
			}
			
			//we found air, place steam here
			if (bA == Blocks.air)
			{
				GlennsGases.placeSteam(w, x, y + ii, z, 8);
				int l = d0.getFluid(this, w.getBlockMetadata(x, y, z), x & 0xF, y, z & 0xF);
				l -= (Settings.FLUID_MAX >> 1);
				W.setFluid(d0, x & 0xF, y, z & 0xF, this, 0, this, Math.max(0, l), 0x7);
			}
		}
		
		
	}
	
	
	//get the fluid for this block
	//@Override
	//public Fluid getFluid() { return FluidRegistry.WATER; }
	
	 /*
     * The game crashes if some of these methods are not present. They should be inherited, but
     * apparently this is not guaranteed. 
     */
	
	
    @Override public boolean func_149698_L() { return true; }
    @SideOnly(Side.CLIENT) public boolean getCanBlockGrass() { return this.canBlockGrass; }
    @Override public Block setHardness(float f){ return super.setHardness(f);  }
    public Block c(float f) { this.blockHardness = f; return this; }
    @Override public Block setLightOpacity(int o) { return super.setLightOpacity(0); }
    @Override public Block setBlockName(String name) { return super.setBlockName(name);  }
    @Override public Block setLightLevel(float f) { return super.setLightLevel(f); }
    @Override public Block setBlockTextureName(String tex) { return super.setBlockTextureName(tex); }
    @Override public Block disableStats() { return super.disableStats(); } 
    
    //The following fields and methods are contained within BlockWater. They should be here just
    //so that other things don't explode.
    int field_149815_a;
    boolean[] field_149814_b = new boolean[4];
    int[] field_149816_M = new int[4];
    private void func_149811_n(World p_149811_1_, int p_149811_2_, int p_149811_3_, int p_149811_4_) { return;  }
    private void func_149813_h(World p_149813_1_, int p_149813_2_, int p_149813_3_, int p_149813_4_, int p_149813_5_) { return; }
    private int func_149812_c(World p_149812_1_, int p_149812_2_, int p_149812_3_, int p_149812_4_, int p_149812_5_, int p_149812_6_) { return 0; }
    private boolean[] func_149808_o(World p_149808_1_, int p_149808_2_, int p_149808_3_, int p_149808_4_) { return null; }
    private boolean func_149807_p(World p_149807_1_, int p_149807_2_, int p_149807_3_, int p_149807_4_){ return false; }
    protected int func_149810_a(World p_149810_1_, int p_149810_2_, int p_149810_3_, int p_149810_4_, int p_149810_5_) { return 0; }
    private boolean func_149809_q(World p_149809_1_, int p_149809_2_, int p_149809_3_, int p_149809_4_) { return false; }
    
	
}
