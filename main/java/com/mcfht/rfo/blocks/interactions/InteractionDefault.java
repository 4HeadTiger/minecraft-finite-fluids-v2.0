package com.mcfht.rfo.blocks.interactions;

import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;

/***
 * 
 * The default fluid interaction. Handles displacement of heavier fluids.
 * 
 * @author FHT
 *
 */
public class InteractionDefault extends Interaction {

	public static final Interaction instance = new InteractionDefault();
	
	@Override
	public int doInteraction(IChunk d0, BlockFiniteFluid f0, int l0, int x0, int y0, int z0, 
								 IChunk d1, BlockFiniteFluid f1, int l1, int x1, int y1, int z1) 
	{
		
		if (f0.density <= f1.density || f0.getFluid() == f1.getFluid()) return 0;
		
		if (x0 == x1 && z0 == z1)
		{
			if (y0 > y1) //if and only if we are above lolz
			{
				d0.getIWorld().setFluid(d0, x0, y0, z0, f0, -1, f1, l1, 0x7);
				d1.getIWorld().setFluid(d1, x1, y1, z1, f1, -1, f0, l0, 0x7);
				
				if (y0 < 255)
				{
					d0.setUpdateImmediate(x0 & 0xF, y0 + 1, z0 & 0xF, false);
					d0.setUpdate(x0 & 0xF, y0 + 1, z0 & 0xF, true);
				}
				return -1;
			}
		}
		else if (l0 > (f0.getMinBlock(d0.getIWorld().getWorld()) << 1))
		{
			if (f1.displace(d1, l1, x1, y1, z1, 48, false, true))
			{
				l0 = (l0 + 1) >> 1;
				d0.getIWorld().setFluid(d0, x0, y0, z0, f0, -1, f0, l0, 0x7);
				d1.getIWorld().setFluid(d1, x1, y1, z1, f1, -1, f0, l0, 0x7);
			}
			else
			{
				d0.getIWorld().setFluid(d0, x0, y0, z0, f0, -1, f1, l1, 0x7);
				d1.getIWorld().setFluid(d1, x1, y1, z1, f1, -1, f0, l0, 0x7);
			}
			return -1;

		}
		return 0;
	}

}
