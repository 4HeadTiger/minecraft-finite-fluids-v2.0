package com.mcfht.rfo.blocks.interactions;

import com.mcfht.rfo.Settings;
import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.scheduler.deferred.BlockIgnition;

import net.minecraft.block.material.Material;

/** This causes lava and flammable fluids to burn really hard. Needs improving at some point. */
public class InteractionLavaFlammable extends Interaction {

	
	final float power;
	public InteractionLavaFlammable() { power = 1.0F; }
	public InteractionLavaFlammable(float explosiveness) { power = explosiveness; }
	
	@Override
	public int doInteraction(IChunk d0, BlockFiniteFluid f0, int l0, int x0, int y0, int z0, IChunk d1,
			BlockFiniteFluid f1, int l1, int x1, int y1, int z1) {
		
		if (f0.getMaterial() == Material.lava) new BlockIgnition(d1.getIWorld().getWorld(), x1, y1, z1, f1.ffluid.flammability, l1);
		else new BlockIgnition(d0.getIWorld().getWorld(), x0, y0, z0, f0.ffluid.flammability, l0);

		d0.getIWorld().setFluid(d0, x0, y0, z0, f0, 0, f0, 0, 0x7);
		d1.getIWorld().setFluid(d1, x1, y1, z1, f1, 0, f1, 0, 0x7);

		return -1;
	}

	/** The power with which to explode. */
	public float getPower(int level)
	{
		float l = Math.max(level, Settings.FLUID_MAX >> 2);
		l *= power / Settings.FLUID_MAX;
		return Math.max(0.5F, l);
	}
	
	
}
