package com.mcfht.rfo.blocks.interactions;

import com.mcfht.rfo.Settings;
import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.compatibility.GlennsGases;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.scheduler.deferred.CreateSteam;
import com.mcfht.rfo.utils.Coord;
import com.mcfht.rfo.utils.Util;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;

/**
 * This class handles all interactions between water and lava.
 * @author FHT
 *
 */
public class InteractionWaterLava extends Interaction 
{

	public static final Interaction instance = new InteractionWaterLava();
	
	@Override
	public int doInteraction(IChunk d0, BlockFiniteFluid f0, int l0, int x0, int y0, int z0, 
								 IChunk d1, BlockFiniteFluid f1, int l1, int x1, int y1, int z1) 
	{
		//get a random seed (this method is very efficient)
		int seed = Util.randomDirVal4();
		int lavaLevel = f0.getMaterial() == Material.water ? l1 : l0;
		
		//Water/Lava x/y/z/level
		IChunk LD = d1, WD = d0;
		
		
		Coord cW = new Coord(x0, y0, z0), cL = new Coord(x1, y1, z1);
		int lW = l0; int lL = l1;
		
		
		if (f0.getMaterial() == Material.lava)
		{
			lW = l1; lL = l0;
			cW.set(x1,y1,z1);
			cL.set(x0,y0,z0);
			LD = d0; WD = d1;

		}

		Block bL = lL >= (Settings.FLUID_MAX - (Settings.FLUID_LAYER / (seed + 1))) ? Blocks.obsidian : (seed & 0x1) == 0 ? Blocks.stone : Blocks.cobblestone;
		
		//the lava becomes air
		LD.getIWorld().setBlock(LD, cL.x, cL.y, cL.z, Blocks.air, 0, 0xF);
		LD.setFluidData(cL.x & 0xF, cL.y, cL.z & 0xF, 0);
		LD.setPressure(cL.x & 0xF, cL.y, cL.z & 0xF, 0);

		//eat half a block of water
		lW -= Settings.FLUID_MAX >> 1;
		WD.getIWorld().setFluid(WD, cW.x, cW.y, cW.z, Blocks.water, 0, (BlockFiniteFluid)Blocks.water, Math.max(0, lW), 0xF);
		
		//the destination always turns to stone/obsidian
		d1.getIWorld().setBlock(d1, x1, y1, z1, bL, 0, 0xF);
		d1.setFluidData(x1 & 0xF, y1, z1 & 0xF, 0);
		d1.setPressure(x1 & 0xF, y1, z1 & 0xF, 0);

		//glenn's gasses support - make steam yo
		if (GlennsGases.INSTALLED && y0 < 255 && y1 < 255)
		{
			new CreateSteam(WD.getIWorld().getWorld(), cW.x, Math.max(y0, y1), cW.z, Math.max(3, Math.min(16, lW / (Settings.FLUID_MAX >> 5))));
		}
		
		return -1; //stop processing now
	}

}
