package com.mcfht.rfo.blocks.interactions;

import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;

public class Interaction 
{

	public final int perform(IChunk d0, BlockFiniteFluid f0, int l0, int x0, int y0, int z0,
						     IChunk d1, BlockFiniteFluid f1, int l1, int x1, int y1, int z1)
	{
		
		//post interaction start
		
		int r = doInteraction(d0, f0, l0, x0, y0, z0, d1, f1, l1, x1, y1, z1 );
		
		//post interaction end
		
		return r;
	}
	
	/**
	 * This handles interactions between two different fluids. 
	 * <p>Returns are the trickiest part. Return of 0, indicates that we behave as it
	 * l1 is a solid block. Returning -1, will indicate that the flow method is to exit
	 * without doing any more actions of any kind. Returning 0 or 1, indicates that the
	 * interacting block (f0) remains intact.
	 * 
	 * <p><b>If l0 or f0 is changed, this method MUST return -1.</b>.
	 */
	public int doInteraction(IChunk d0, BlockFiniteFluid f0, int l0, int x0, int y0, int z0,
							 IChunk d1, BlockFiniteFluid f1, int l1, int x1, int y1, int z1 ) { return 0; }
	
}
