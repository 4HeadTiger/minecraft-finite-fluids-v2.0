package com.mcfht.rfo.blocks;

import com.mcfht.rfo.datastructure.Manager;
import com.mcfht.rfo.utils.Observer;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.World;

public class BlockObserver extends Block {

	/** How far this observer can see*/
	public int range;
	
	public BlockObserver(Material mat, int range) {
		super(mat);
		this.range = range;
	}
	
	@Override
	public void onBlockAdded(World w, int x, int y, int z) 
	{
		Manager.instance.getOrRegisterWorld(w).registerObserver(new Observer(x, y, z, range));
		super.onBlockAdded(w, x, y, z);
	}
	
	@Override
	public void breakBlock(World w, int x, int y, int z, Block b, int m) 
	{
		Manager.instance.getOrRegisterWorld(w).deregisterObserver(new Observer(x, y, z, range));
		super.breakBlock(w, x, y, z, b, m);
	}
	

	

}
