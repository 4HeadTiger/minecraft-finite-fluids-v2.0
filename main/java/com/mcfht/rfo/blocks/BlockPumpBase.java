package com.mcfht.rfo.blocks;

import java.util.Random;

import com.mcfht.rfo.RFORegistry;
import com.mcfht.rfo.Settings;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.datastructure.DataInterfaces.IWorld;
import com.mcfht.rfo.tileentities.TileEntityPump;
import com.mcfht.rfo.utils.Coord;
import com.mcfht.rfo.utils.Util;
import com.mcfht.rfo.utils.Coord.Direction;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;
import net.minecraftforge.fluids.Fluid;

public class BlockPumpBase extends PressureProvider 
{

	public IIcon icon0;
	public IIcon icon1;
	public int tier;
	
	public BlockPumpBase(Material mat, int PressureMax, int Tier) 
	{
		super(mat, PressureMax);
		this.setTickRandomly(true);
		this.tier = Tier;
	}
	
	@Override
	public void registerBlockIcons(IIconRegister reg) {
		icon0 = reg.registerIcon("piston_inner");
		icon1 = reg.registerIcon("furnace_top");
		
	}
	
	@Override public IIcon getIcon(int s, int m) 
	{
		if (s == (m & 0x7)) return icon0;
		return icon1;
	};
	
	@Override
	public TileEntity createNewTileEntity(World w, int m)
    {
        return new TileEntityPump(0);
    }
	
	@Override
	public boolean updateNeighbors(IWorld w, IChunk d0, int x0, int y0, int z0, int m0)
	{
		//System.out.println(" ===> Updating PP <=== ===> [" + x0 + ", " + y0 + ", " + z0 + "] <===");
		//init some temp variables
		boolean pushed = false;
		//get the right world and chunk objects
		

		Fluid f = ((TileEntityPump) w.getWorld().getTileEntity(x0, y0, z0)).f;

		Coord c1 = new Coord(x0, y0, z0).step(Direction.get(m0 & 0x7));
		
		IChunk d1 = w.validateChunkData(d0, c1.x, c1.z, false);
		if (f == null || d1 == null || c1.y > 255 || c1.y < 0) return false;
		Block b1 = d1.getBlock(c1.x & 0xF, c1.y, c1.z & 0xF);
		
		//get the level of this block
		int l1 = 0;
		//if it's the same fluid, get the fluid level of this block
		if (b1 instanceof BlockFiniteFluid && ((BlockFiniteFluid)b1).getFluid() == f) l1 = d0.getFluid(b1, -1, c1.x & 0xF, c1.y, c1.z & 0xF);
		
		//If we have a valid fluid in this space
		if (l1 >= Settings.FLUID_MAX)
		{
			int p1 = Math.max(d1.getPressure(c1.x & 0xF, c1.y, c1.z & 0xF), pressureMax - 1 - ((c1.y - y0) * Settings.PRESSURE_STEP));
			
			d1.setPressure(c1.x & 0xF, c1.y, c1.z & 0xF, p1);
			d1.setUpdate(c1.x & 0xF, c1.y, c1.z & 0xF, true);
			d1.setUpdateImmediate(c1.x & 0xF, c1.y, c1.z & 0xF, true);
			
			pushed = true;
		}
		
		return pushed;
	}
	
	//really messy... should be fine
	@Override
	public void onBlockPlacedBy(World w, int x, int y, int z, EntityLivingBase p, ItemStack is)
    {
		double dx = (double)x - p.posX;
		double dy = (double)y - p.posY;
		double dz = (double)z - p.posZ;
		
		int m;
		//there are 6 directions, this should do the trick.
		if (Math.abs(dz) > Math.abs(dx))
		{
			if (Math.abs(dy) > Math.abs(dz))
			{
				if (dy > 0) { m = 0; } //0:-1
				else { m = 1;} //0:+1
			}
			else if (dz > 0) { m = 2; } //0:-1
			else { m = 3;} //0:+1
		}
		else
		{
			if (Math.abs(dy) > Math.abs(dx))
			{
				if (dy > 0) { m = 0; } //0:-1
				else { m = 1;} //0:+1
			}
			else if (dx > 0) { m = 4; } //+1:0
			else { m = 5; } //-1:0
		}
		w.getChunkFromChunkCoords(x >> 4, z >> 4).setBlockMetadata(x & 0xF, y, z & 0xF, m);
    }
	
	
	@Override
	public int pull(IWorld W, IChunk d, int x, int y, int z, int amount)
	{
		int m = d.getMetadata(x & 0xF, y, z & 0xF);
		TileEntityPump tep = (TileEntityPump) W.getWorld().getTileEntity(x, y, z);
		return tep.pullFromBuffer(d);
	}
	
	
	@Override
	public int tryProvide(IWorld W, IChunk d, int x, int y, int z, Fluid f)
	{
		//only if powered
		if (W.getWorld().isBlockIndirectlyGettingPowered(x, y, z) || W.getWorld().isBlockIndirectlyGettingPowered(x, y + 1, z))
		{
			int m = d.getMetadata(x & 0xF, y, z & 0xF);
			if ((m & 0x8) != 0)
			{
				TileEntityPump tep = (TileEntityPump) W.getWorld().getTileEntity(x, y, z);
				if (tep.buffer > 0 && tep.f != null && tep.f == f)
				{
					return tep.maxFromBuffer(d);
				}
			}
		}
		return 0;
	}
	

	
}
