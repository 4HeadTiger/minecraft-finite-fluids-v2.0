package com.mcfht.rfo.datastructure;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.ReentrantLock;

import com.mcfht.rfo.RFO;
import com.mcfht.rfo.Settings;
import com.mcfht.rfo.blocks.BlockFiniteFluid;
import com.mcfht.rfo.blocks.PressureProvider;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.datastructure.DataInterfaces.IWorld;
import com.mcfht.rfo.utils.Coord;
import com.mcfht.rfo.utils.Util;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagByteArray;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.storage.ExtendedBlockStorage;

public class ChunkData implements IChunk {

	/**  Contains fluid data for the blocks  */
	protected AtomicReferenceArray<int[]> fluidData;
	
	/** Contains the update data for the blocks. Changes made to this array will only be seen
	 * during the NEXT update sweep. */
	protected AtomicReferenceArray<boolean[]> updates;
	protected boolean[] sectionHasUpdates = new boolean[16];
	
	/** Contains the working update data. Updates in this array are seen only during an update sweep
	 * and therefore only HIGHER updates will be seen by the updater. */
	protected AtomicReferenceArray<boolean[]> updatesWorking;
	
	/** Contains all the pressure data for this chunk. */
	protected AtomicReferenceArray<short[]> pressureData;
	
	/** The blocks that have changed in this chunk */
	protected AtomicReferenceArray<boolean[]> blockChangeFlags;
	
	/** The IWorld containing this chunk data*/
	public IWorld W;
	/** The Minecraft chunk corresponding to this data object */
	public Chunk c;
	
	/** The <b>chunk coordinates</b> of this data object. */
	public final int xPos, zPos;
	
	/** The pos of this chunk for reasons, mostly easier hashing etc. */
	public final Coord pos;
	
	/** The dist to the nearest player */
	public volatile int dist;
	
	/** The time since we were last sent (in ticks/update ticks) */
	public volatile int tCounter;
	
	/** For reasons. Lock maybe. Nice to have. */
	public final ReentrantLock lock = new ReentrantLock(false);
	
	/** Whether this chunk has <b>f</b>luid or <b>c</b>lient updates. */
	public volatile boolean fUpdates = false, cUpdates = false; 
	
	/** Whether pressure is invalidated */
	public volatile boolean pressureInvalidated = false;
	
	public ChunkData(IWorld w, Chunk chunk)
	{
		W = w;
		c = chunk;
		xPos = c.xPosition;
		zPos = c.zPosition;
		pos = new Coord(this);
		
		fluidData = new AtomicReferenceArray<int[]>(16);
		updates = new AtomicReferenceArray<boolean[]>(16);
		updatesWorking = new AtomicReferenceArray<boolean[]>(16);
		pressureData =  new AtomicReferenceArray<short[]>(16);
		blockChangeFlags = new AtomicReferenceArray<boolean[]>(16);
	}
	
	@Override public int hashCode() { return pos.hashCode(); }
	@Override public boolean equals(Object o) { return pos.equals(o); }
	
	/* Some temp variables for construction reasons */
	public volatile boolean[] tempBool;
	public volatile short[] tempShort;
	public volatile int[] tempInt;
	public volatile ExtendedBlockStorage ebsTemp;
	
	
	@Override public IWorld getIWorld() { return W; }
	@Override public Chunk getChunk() { return c; }

	@Override public int xPos() { return xPos; }

	@Override public int zPos() { return zPos; }

	/** It is assumed that this distance is SQUARED.<hr>{@inheritDoc}<p>*/
	@Override public int getDist() { return dist; }

	@Override public boolean hasClientUpdates() { return cUpdates; }
	
	@Override public boolean hasFluidUpdates() {return fUpdates; }
	
	@Override public int ticksPassed() { return tCounter; }

	@Override public void elapseTick() { tCounter++; }
	
	/** It is assumed that this distance is SQUARED.<hr>{@inheritDoc}<p>*/
	@Override
	public void updateDist(int dist) {
		this.dist = Math.min(this.dist, dist);
		if (this.dist < 0) this.dist = 0;
	}

	@Override
	public void acquire() {
		lock.lock();
	}

	@Override
	public void release() {
		lock.unlock();

	}

	@Override
	public boolean canAcquire(long timeout) {
		if (lock.isLocked() == false || lock.isHeldByCurrentThread()) return true;
		return true;
	}



	@Override
	public boolean prepareForSending() 
	{
		tCounter = 0;
		cUpdates = false;
		return true;
	}


	@Override
	public void chunkSending()
	{
		if (cUpdates == false)
			dist = 999;
	}
	
	/**
	 * Contains the bounds for the x->z iteration
	 */
	static final int loopBounds[][] = {
			{0, 16, 1, 0, 16, 1},
			{0, 16, 1, 15, -1, -1},
			{15, -1, -1, 0, 16, 1},
			{15, -1, -1, 15, -1, -1}
	};
	
	@Override
	public int performUpdates() 
	{
		//System.out.println(" Starting Chunk Update at: " + pos);
		
		//copy the updates into the working array
		copyUpdates();
		
		fUpdates = false;
		
		//empty pressure again if necessary
		if (Settings.PRESSURE_ENABLED && pressureInvalidated) 
		{
			synchronized(this) 
			{ 
				for (int i = 0; i < 16; i++)
				{
					if (pressureData.get(i) != null) Arrays.fill(pressureData.get(i), (short)0);
				}
			}
			pressureInvalidated = false;
		}	
		
		for (int ey = 0; ey < 16; ey++)
		{
			if (sectionHasUpdates[ey] == false) continue;
			sectionHasUpdates[ey] = false; //skip doing sections that do not have updates
			if (updatesWorking.get(ey) == null) continue; //coyUpdates puts null here if no updates
			
			//now get the ebs array for this section
			ExtendedBlockStorage ebs = c.getBlockStorageArray()[ey];
			
			//skip trying to update empty EBS
			if (ebs == null || ebs.isEmpty()) continue;

			//lock the EBS object?
			//synchronized(ebs){
			
			
			//now loop through y/x/z psuedo randomly
			for (int cy = 0; cy < 16; cy++)
			{

				int y = cy + (ey << 4);
				final int[] bounds = loopBounds[Util.randomDirVal4()]; //go random direction on each layer
				
				//loop within the random bounds
				for (int cx = bounds[0]; cx != bounds[1]; cx += bounds[2])
				{
					for (int cz = bounds[3]; cz != bounds[4]; cz += bounds[5])
					{
						if (getUpdate(cx, y, cz)) //if there is an update here
						{
							Block b0 = ebs.getBlockByExtId(cx, cy, cz);	//only update fluid blocks
							if (b0 instanceof BlockFiniteFluid)
							{
								//System.out.println("     - got an update!");
								((BlockFiniteFluid)b0).updateFlow(W, this, cx + (xPos << 4), y, cz + (zPos << 4), ebs.getExtBlockMetadata(cx, cy, cz));
							}
							else if (b0 instanceof PressureProvider)
							{
								((PressureProvider)b0).updatePressure(W, this, cx + (xPos << 4), y, cz + (zPos << 4), ebs.getExtBlockMetadata(cx, cy, cz));
							}
						}
					}
				}
			}
		}
		
		
		//Now recalculate lighting on this chunk
		
		//skylightColumnIndex = z << 4 | x
		
		
		//empty pressure again if necessary
		if (Settings.PRESSURE_ENABLED && pressureInvalidated) 
		{
			synchronized(this) 
			{ 
				for (int i = 0; i < 16; i++)
				{
					if (pressureData.get(i) != null) Arrays.fill(pressureData.get(i), (short)0);
				}
			}
			pressureInvalidated = false;
		}		
		
		return 0;
		
	}
	
	/** Copies this entire ChunkData update array into a new array. Retains null entries where appropriate. */
	public AtomicReferenceArray<boolean[]> copyUpdates()
	{
		for (int i = 0; i < 16; i++)
		{
			copyUpdateSection(i);
		}
		return updatesWorking;
	}
	
	/**
	 * Copies the update section from this ChunkData object. i corresponds to y >> 4.
	 * 
	 * <p>Copies updates {@link ChunkData#updatesWorking} from {@link ChunkData#updates}.
	 */
	public void copyUpdateSection(int ey)
	{
		if (updates.get(ey) == null) 
		{
			updatesWorking.set(ey, null);
			return;
		}
		updatesWorking.set(ey, updates.get(ey).clone());
	}

	@Override
	public void flagBlockUpdate(int cx, int y, int cz) 
	{
		//System.out.println(" -> Flagging a block render update!");
		if (blockChangeFlags.get(y >> 4) == null) 
		{ 
			synchronized(this)
			{
				if (blockChangeFlags.get(y >> 4) == null)
				{
					blockChangeFlags.set(y >> 4, new boolean[4096]);
				}
			}
		}
		
		if (cUpdates == false) 
		{
			cUpdates = true;
			RFO.sender.markChunkDirty(this);
		} 
		
		blockChangeFlags.get(y >> 4)[Util.getIndex12(cx, y & 0xF, cz)] = true;

	}

	@Override
	public boolean[][] getBlockFlags() 
	{
		boolean[][] out = new boolean[16][];
		for (int i = 0; i < 16; i++) out[i] = blockChangeFlags.get(i);
		return out;
	}

	@Override
	public boolean isPressureValid() {
		return Settings.PRESSURE_ENABLED && pressureInvalidated == false;
	}

	@Override
	public void invalidatePressure() {
		pressureInvalidated = true;
	}

	@Override
	public ExtendedBlockStorage getEbs(int ey, boolean safe)
	{
		Chunk c = getChunk();
		ExtendedBlockStorage ebs = c.getBlockStorageArray()[ey];
		
		//if it's null
		if (ebs == null && safe) 
		{
			//lock on this data object
			//prevents other threads McMaking this (may synchronize on the chunk, needs more ASM later)
			synchronized(this)
			{
				//if it's still null (i.e in case it was created between the first null and acquiring the lock)
				if ((ebs = c.getBlockStorageArray()[ey]) == null)
				{
					//create a new one
					ebsTemp = new ExtendedBlockStorage(ey << 4, !W.getWorld().provider.hasNoSky);
					ebs = c.getBlockStorageArray()[ey] = ebsTemp;
					ebsTemp = null;
				}
			}
		}
		return ebs;
	}

	@Override
	public int getMetadata(int cx, int y, int cz) 
	{
		return getEbs(y >> 4, true).getExtBlockMetadata(cx, y & 0xF, cz);
	}

	@Override
	public Block getBlock(int cx, int y, int cz) 
	{
		return getEbs(y >> 4, true).getBlockByExtId(cx, y & 0xF, cz);
	}

	@Override
	public void setMetadata(int cx, int y, int cz, int m) 
	{
		getEbs(y >> 4, true).setExtBlockMetadata(cx, y & 0xF, cz, m);
	}

	@Override
	public void setBlock(int cx, int y, int cz, Block b, int m) 
	{
		ExtendedBlockStorage ebs = getEbs(y >> 4, true);
		if (b != null) ebs.func_150818_a(cx, y & 0xF, cz, b);
		if (m >= 0) ebs.setExtBlockMetadata(cx, y & 0xF, cz, m);
	}

	/** Accepts m0 = -1 if metadata not known. <hr>{@inheritDoc}*/
	@Override
	public int getFluid(Block b0, int m0, int cx, int y, int cz) 
	{
		int l = getFluidData(cx, y, cz);
		if (b0 instanceof BlockFiniteFluid)
		{
			if (l == 0)
			{
				if (m0 < 0) m0 = getMetadata(cx, y, cz);
				setFluidData(cx, y, cz, l = ((BlockFiniteFluid)b0).getLevelFromMeta(m0));
			}
		}
		else
		{
			setFluidData(cx, y, cz, l = 0);
			setPressure(cx, y, cz, 0);
		}
		return l;	
	}

	
	@Override
	public int getFluidData(int cx, int y, int cz) 
	{
		if (fluidData.get(y >> 4) == null) 
		{ 
			synchronized(this)
			{
				if (fluidData.get(y >> 4) == null) 
				{
					fluidData.set(y >> 4, new int[4096]);
					return 0;
				}
			}
		}
		return fluidData.get(y >> 4)[Util.getIndex12(cx, y & 0xF, cz)];
	}
	
	@Override
	public void setFluidData(int cx, int y, int cz, int l) 
	{
		if (fluidData.get(y >> 4) == null) 
		{ 
			synchronized(this)
			{
				if (fluidData.get(y >> 4) == null) 
				{
					fluidData.set(y >> 4, new int[4096]);
				}
			}
		}
		fluidData.get(y >> 4)[Util.getIndex12(cx, y & 0xF, cz)] = l;
	}

	@Override
	public int getPressure(int cx, int y, int cz)
	{
		if (pressureData.get(y >> 4) == null) 
		{ 
			synchronized(this)
			{
				if (pressureData.get(y >> 4) == null) 
				{
					pressureData.set(y >> 4, new short[4096]);
					return 0;
				}
			}
		}
		return pressureData.get(y >> 4)[Util.getIndex12(cx, y & 0xF, cz)];
	}

	@Override
	public void setPressure(int cx, int y, int cz, int p) 
	{
		if (pressureData.get(y >> 4) == null) 
		{ 
			synchronized(this)
			{
				if (pressureData.get(y >> 4) == null) 
				{
					pressureData.set(y >> 4, new short[4096]);
				}
			}
		}
		pressureData.get(y >> 4)[Util.getIndex12(cx, y & 0xF, cz)] = (short)Math.min(Settings.PRESSURE_MAX, p);
	}

	@Override
	public boolean getUpdate(int cx, int y, int cz) 
	{
		/*
		if (updatesWorking.get(y >> 4) == null) 
		{ 
			synchronized(this)
			{
				if (updatesWorking.get(y >> 4) == null) 
				{
					updatesWorking.set(y >> 4, new boolean[4096]);
					return false;
				}
			}
		}*/
		return updatesWorking.get(y >> 4)[Util.getIndex12(cx, y & 0xF, cz)];
	}

	@Override
	public void setUpdate(int cx, int y, int cz, boolean update) {
		if (updates.get(y >> 4) == null) 
		{ 
			synchronized(this)
			{
				if (updates.get(y >> 4) == null) 
				{
					updates.set(y >> 4, new boolean[4096]);
				}
			}
		}
		fUpdates = true; sectionHasUpdates[y >> 4] = true;
		updates.get(y >> 4)[Util.getIndex12(cx, y & 0xF, cz)] = update;
	}

	@Override
	public void setUpdateImmediate(int cx, int y, int cz, boolean update) 
	{
		if (updatesWorking.get(y >> 4) == null) 
		{ 
			synchronized(this)
			{
				if (updatesWorking.get(y >> 4) == null) 
				{
					updatesWorking.set(y >> 4, new boolean[4096]);
				}
			}
		}
		fUpdates = true; sectionHasUpdates[y >> 4] = true;
		updatesWorking.get(y >> 4)[Util.getIndex12(cx, y & 0xF, cz)] = update;
	}
	
	
	public NBTTagCompound toNBT()
	{
		//to be safest
		acquire();
		
		NBTTagCompound nbt = new NBTTagCompound();
		for(int ey = 0; ey < 16; ey++)
		{
			if (fluidData.get(ey) == null) continue;
			
			//nbt.setIntArray("fluid_" + ey, fluidData.get(ey));	
			
			//get the update arrays, a little quicker than a bajillion fenced reads
			boolean[] cFlags = updates.get(ey);
			boolean[] iFlags = updatesWorking.get(ey);
			
			if (cFlags == null && iFlags == null) continue;
			
			//make sure we gots something to copy from;
			if (cFlags == null) cFlags = new boolean[4096];
			if (iFlags == null) iFlags = new boolean[4096];

			
			
			
			//get the pressure and fluid arrays
			short[] pressure = pressureData.get(ey);
			int[] levels = fluidData.get(ey);
			if (pressure == null) pressure = new short[4096];
			if (levels == null) levels = new int[4096];

			//and something to copy into
			int[] fluid = new int[4096];
			byte[] flags = new byte[4096];
			for (int i = 0; i < 4096; i++) 
			{
				fluid[i] = (pressure[i] << 16) | levels[i];
				flags[i] = (byte) ((cFlags[i] || iFlags[i]) ? 1 : 0);
			}
			
			nbt.setIntArray("fluid_" + ey, fluid);
			nbt.setByteArray("fupdate_" + ey, flags);
			
		}
		release();
		return nbt;
	}
	
	
	public void fromNBT(NBTTagCompound nbt)
	{
		acquire();
		for(int ey = 0; ey < 16; ey++)
		{
			//if we have fluid levels to rea
			if (nbt.hasKey("fluid_" + ey))
			{
				int[] read = nbt.getIntArray("fluid_" + ey);
				short[] pressure = null;
				int[] fluid = null;
				if (read != null)
				{
					pressure = new short[4096];
					fluid = new int[4096];
					for (int i = 0; i < 4096; i++)
					{
						pressure[i] = (short) (read[i] >> 16);
						fluid[i] = (read[i] & 0x3FFF);
					}

				}
				fluidData.set(ey, fluid);	
				pressureData.set(ey, pressure);				
			}
			
			//if we have fluid updates to read
			if (nbt.hasKey("fupdates_" + ey))
			{
				byte[] flags = nbt.getByteArray("fupdates_" + ey);
				for (int i = 0; i < 4096; i++) if (flags[i] != 0) updates.get(ey)[i] = true;
			}
		}
		release();
	}

}
