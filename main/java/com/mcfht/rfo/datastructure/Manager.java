package com.mcfht.rfo.datastructure;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import com.mcfht.rfo.RFO;
import com.mcfht.rfo.Settings;
import com.mcfht.rfo.datastructure.DataInterfaces.IChunk;
import com.mcfht.rfo.datastructure.DataInterfaces.IManager;
import com.mcfht.rfo.datastructure.DataInterfaces.IWorld;
import com.mcfht.rfo.scheduler.FluidUpdater;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.event.world.ChunkDataEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.event.world.WorldEvent.Load;
import net.minecraftforge.event.world.WorldEvent.Unload;

public class Manager implements IManager {

	public static final IManager instance = new Manager();
	

	/**
	 * This map contains a list of all of the worlds that are loaded by this instance
	 */
	public ConcurrentHashMap<World, IWorld> worldCache = new ConcurrentHashMap<World, IWorld>(16, 0.33F);
	
	@Override
	public Collection<IWorld> getWorlds()
	{
		return worldCache.values();
	}
	
	@SubscribeEvent
	public void worldLoad(Load e) {
		getOrRegisterWorld(e.world);
		//if (worldCache.size() > 0) RFO.threadManager.resume();
	}

	@SubscribeEvent
	public void worldUnload(Unload e) {
		IWorld w = deregisterWorld(e.world);
		FluidUpdater.chunksDone.set(0);
		RFO.sweepCounter = 0;

	}

	@SubscribeEvent
	public void chunkLoad(ChunkEvent.Load e) {
		getOrRegisterWorld(e.world).getOrRegisterChunk(e.getChunk());
	}

	@SubscribeEvent
	public void chunkUnload(ChunkEvent.Unload e) {
		getOrRegisterWorld(e.world).deregisterChunk(e.getChunk());
	}
	
	@SubscribeEvent
	public void chunkSave(ChunkDataEvent.Save e)
	{
		if (Settings.SAVE_NBT)
		{
			IChunk C = getOrRegisterWorld(e.world).getChunk(e.getChunk());
			if (C != null)
			{
				NBTTagCompound nbt = C.toNBT();
				e.getData().setTag("FF_fluidData", nbt);
			}
		}
	}
	
	/** Give the chunk an opportunity to load and store data. */
	@SubscribeEvent
	public void chunkRead(ChunkDataEvent.Load e)
	{
		if (Settings.SAVE_NBT)
		{
			IChunk C = getOrRegisterWorld(e.world).getOrRegisterChunk(e.getChunk());
			if (C != null && e.getData().hasKey("FF_fluidData"))
			{
				C.fromNBT(e.getData().getCompoundTag("FF_fluidData"));
			}
		}
	}

	public IWorld getOrRegisterWorld(World w) 
	{
		IWorld W;
		if ((W = worldCache.get(w)) != null) return W;
		worldCache.put(w, W = new WorldData(w)); //change this to replace entire DS.
		return W;	
	}

	@Override
	public IWorld getWorld(World w) {
		return worldCache.get(w);
	}

	@Override
	public IWorld deregisterWorld(World w) {
		return worldCache.remove(w);
	}

}
